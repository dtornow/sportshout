import { NgModule } from '@angular/core';

import {AutosizeDirectrive} from './autosize/autosize';

@NgModule({
  declarations: [
    AutosizeDirectrive
  ],
  imports: [
  ],
  exports: [
    AutosizeDirectrive
  ]
})
export class DirectrivesModule {} 