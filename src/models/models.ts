import { FirebaseObjectObservable } from 'angularfire2/database';

export const CHAT_MESSAGE_TEXT = 0;
export const CHAT_MESSAGE_SUBJECT = 1;
export const CHAT_MESSAGE_USER_LEAVED = 2;
export const CHAT_MESSAGE_USER_DELETED = 3;

export interface AppSettings {
    show: string;
    searchRange: number;
    ageRange: { lower: number, upper: number }
    notifyShout: boolean;
    notifyMessage: boolean;
}


export class UserData {
    uid: string;
    name: string;
    email: string;
    provider: string;
    image: string;

    gender?: string;
    birthdate?: string;

    needChangeBirthday?: boolean

    osUserId?:string;
    osPushToken?:string;
}

export class User {
    $key?: string;
    name?: string;
    email?: string;
    provider?: string;
    image?: string;

    gender?: string;
    birthdate?: string;
    needChangeBirthday?: boolean

    info?: string

    osUserId?:string;
    osPushToken?:string;
}

export interface Shout {
    $key?: string;

    content?: string;
    createDate?: number;

    userId?: string;
    //  name?: string;
    //image?: string;
    user?: {
        name?: string;
        image?: string;
        gender?: string;
        birthdate?: string;
    }

    distance?: number;
}

export interface UserShout {
    $key?: string;
    content?: string;
    createDate?: number;
}

export interface UserBlock {
    $key?: string;
    createDate?: number;
}

export interface ShoutBlock {
    $key?: string;
    type?: string;
    createDate?: number;
}

export interface ShoutRequest {
    $key?: string;

    createDate?: number;
    // name?: string;
    // userId?: string;
    isNew?: boolean;
    // image?: string; 
    subject?: string;
    users?: string[];

}


export interface Chat {
    $key?: string;

    subject?: string;
    image?: string;
    createDate?: number;
    updateDate?: number;

    name?: string;
    ownerId?: string;
    //users?: string[];
}

export interface UserChat {
    $key?: string;
    unreadMessages?: number;
    updateDate?: number;

    chat?: FirebaseObjectObservable<Chat>;
}

export interface ChatMessage {
    $key?: string;

    type: number;       // 0: message, 1: leafe chat, 2: etc
    userId?: string;

    createDate?: number;
    message?: string;

    user?: User;
}

export interface ImageInfo {
    name?: string;
    format?: string;
}
