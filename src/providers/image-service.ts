import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';

import { ImageInfo } from '../models/models';

import { environment } from '../environments/environment';


// import * as firebase from 'firebase/app';
 
@Injectable()
export class ImageService {

  static readonly CLAUDINARY_API_URL = environment.cloudinary.apiUrl;
  static readonly CLAUDINARY_API_AUTH_URL = environment.cloudinary.apiAuthUrl;
  static readonly CLAUDINARY_RES_URL = environment.cloudinary.resUrl;


  private options: CameraOptions = {
    quality: 50,
    //    destinationType: this.camera.DestinationType.DATA_URL,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    saveToPhotoAlbum: false,
    correctOrientation: true,
    targetWidth: 500,
    targetHeight: 500,
    allowEdit: true
  };

  constructor(private camera: Camera, public platform: Platform, private transfer: Transfer, public http: Http) {
  }

  public choosePicture(): Promise<string> {
    this.options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;
    return this.camera.getPicture(this.options).then((image) => {
      return image;
    });
  }

  // public choosePictureAndUpload(index: number): Promise<ImageInfo> {
  //   return this.userService.getUser().then((user) => {
  //     this.options.sourceType = this.camera.PictureSourceType.PHOTOLIBRARY;

  //     return this.camera.getPicture(this.options).then((image) => {
  //       console.log("Get Picture returned ", image);
  //       //return this.uploadImageFirebase(user.$key, index, imageData);
  //       return this.uploadImageCloudinary(user.$key, index, image);


  //     });
  //   });
  // }
  // uploadImageFirebase(name, pictureName, data): Promise<ImageInfo> {
  //   let promise = new Promise((res, rej) => {
  //     // let fileName = name + ".jpg";
  //     let uploadTask = firebase.storage().ref(`user-pictures/${name}/${pictureName}.png`).putString(data, 'base64');
  //     uploadTask.on('state_changed', (snapshot) => {
  //     }, (error) => {
  //       rej(error);
  //     }, () => {
  //       var downloadURL = uploadTask.snapshot.downloadURL;
  //       res({ name: `${pictureName}.png`, type: 'fb' });
  //     });
  //   });
  //   return promise;
  // }

  // getFirebaseDownloadUrl(name, pictureName) {
  //   return firebase.storage().ref(`user-pictures/${name}`).child(`${pictureName}`).getDownloadURL();
  // }



  uploadImageCloudinary(name, pictureName, filePath): Promise<ImageInfo> {
    const fileTransfer: TransferObject = this.transfer.create();
    let options: FileUploadOptions = {
      fileKey: 'file',
      //fileName: 'name.jpg',
      params: { file: filePath, upload_preset: 'unsigned' }
    }

    let promise = new Promise((res, rej) => {
      let uri = `${ImageService.CLAUDINARY_API_URL}/image/upload`;

      fileTransfer.upload(filePath, uri, options)
        .then((data: any) => {
          console.log("Success", data);
          let result = JSON.parse(decodeURIComponent(data.response));
          res({ name: result.public_id, format: result.format });

          // success
        }, (err) => {
          console.log("Error", err);
          rej(err);
        })

    });
    return promise;
  }

  uploadFbCloudinary(filePath): Promise<ImageInfo> {
    let promise = new Promise((res, rej) => {
      if (filePath != null) {
        let uri = `${ImageService.CLAUDINARY_API_URL}/image/upload`;

        let data = new URLSearchParams();
        data.append('file', filePath);
        data.append('upload_preset', 'unsigned');
        this.http.post(uri, data, {}).map((res: Response) => res.json()).subscribe(result => {
          console.log("uploadFbCloudinary: Result = ", result)
          res({ name: result.public_id, format: result.format });
        }, (err) => {
//          console.log("uploadFbCloudinary: Error", err);
          rej(err);
        });

      }
      else {
        rej(new Error("uploadFbCloudinary: Error Image == null"));
      }
    });
    return promise;
  }
  public deletePicture(name) {
    // let uri = `${ImageService.CLAUDINARY_API_AUTH_URL}/image/destroy`;


    /*
        let data = new URLSearchParams();
        data.append('public_id', name);
        data.append('api_key', '789826879444916');
        data.append('signature', 'LSHffyQqmi0M2BlB2-9dWZLei7k');
    let headers = new Headers({ 'Content-Type': 'application/json', 'X-Requested-With': 'XMLHttpRequest' });
    let options = new RequestOptions({ headers: headers });
        this.http.post(uri, data, options).map((res: Response) => res.json()).subscribe(result => {
          console.log("Result = ", result)
        });
    */

  }

  public getImageUrl(publicId: string): string {
    return `${ImageService.CLAUDINARY_RES_URL}/image/upload/${publicId}`;
  }
  public getThumbUrl(publicId: string): string {
    return `${ImageService.CLAUDINARY_RES_URL}/image/upload/t_thumb/${publicId}`;
  }

}
