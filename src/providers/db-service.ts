import { Injectable } from '@angular/core';
import 'rxjs/add/operator/toPromise';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { User, ImageInfo, UserShout, UserChat, Chat, ChatMessage, ShoutBlock, ShoutRequest, CHAT_MESSAGE_USER_LEAVED, CHAT_MESSAGE_USER_DELETED } from '../models/models';

import * as firebase from 'firebase/app';
import * as moment from 'moment';
// import * as GeoFire from "geofire";


@Injectable()
export class DbService {

  public static MAX_SHOUTS = 3;
  public static SHOUT_DURATION_IN_WEEKS = 2;


  static readonly USERS = "users";
  static readonly USER_PICTURES = "user-pictures";
  static readonly USER_MODIFIED = "user-modified";
  static readonly USER_MESSAGES = "user-messages";

  static readonly PICTURES_DELETED = "pictures-deleted";

  static readonly SHOUTS = "shouts";
  static readonly USER_SHOUTS = "user-shouts";
  static readonly SHOUT_LOCATIONS = "shout-locations";
  static readonly SHOUT_REQUEST = "shout-request";
  static readonly SHOUT_REQUEST_USERS = "shout-request-users";

  static readonly SHOUT_BLOCK = "shout-block";
  static readonly USER_BLOCK = "user-block";

  static readonly CHATS = "chats";
  static readonly CHAT_USERS = "chat-users";
  static readonly USER_CHATS = "user-chats";
  static readonly CHAT_MESSAGES = "chat-message";

  static readonly REPORT_USER = "report-user";

  constructor(private db: AngularFireDatabase, private afAuth: AngularFireAuth) {
  }


  /* *****************************************
   Photos
  ***************************************** */

  public getUserPhotos(key: string): Promise<ImageInfo[]> {
    //    return new Promise<string[]>((resolve, reject) => {
    let images = [];
    return this.db.object(`${DbService.USER_PICTURES}/${key}/photos`).map((obj: ImageInfo[]) => {
      console.log("Got from db", obj);
      if ((<any>obj).$exists()) {
        images = obj.filter((n) => { return n.name != null && n.name.length > 0; });
      }
      else {
        images = [];
      }
      //        resolve(images);
      return images;
    }).take(1).toPromise();



    //    });
  }

  public saveUserPhotos(user: User, photos: ImageInfo[]) {
    if (photos != null) {
      let cleanedPhotos = photos.filter((n) => { return n != null && n.name != null && n.name.length > 0 });
      let primaryImage: ImageInfo = cleanedPhotos.length > 0 ? cleanedPhotos[0] : { name: user.gender === "male" ? "m" : "f" };

      let deletedPhotos: ImageInfo[] = [];

      // need to change primary image !
      let changePrimary = cleanedPhotos.length > 0;
      this.db.object(`${DbService.USER_PICTURES}/${user.$key}/photos`).take(1).subscribe((obj: ImageInfo[]) => {
        if ((<any>obj).$exists()) {
          changePrimary = obj[0].name !== primaryImage.name;

          obj.forEach((ele) => {
            let fnd = cleanedPhotos.find((ph) => { return ph ? ph.name === ele.name : null });
            if (!fnd) {
              deletedPhotos.push(ele);
            }

          })

          // add images to delete list, so we can delete these later in the admin console 
          this.addToImageDeleteList(deletedPhotos);
        }


        if (changePrimary) {
          console.log("Need to change primary image !", primaryImage);
          this.changePrimaryPhoto(user.$key, primaryImage);
        }
        this.db.object(`${DbService.USER_PICTURES}/${user.$key}/photos`).set(cleanedPhotos);

      });
    }
  }




  // todo; return promise / refactor function! call firebase.database().ref().update only once
  private changePrimaryPhoto(userKey: string, photo: ImageInfo) {

    let photoName = null;
    if (photo != null && photo.name != "") {
      photoName = photo.name;
    }

    // change user photo 
    this.db.object(`${DbService.USERS}/${userKey}/image`).set(photoName);

    // change all shouts
    this.db.list(`${DbService.USER_SHOUTS}/${userKey}`).take(1).subscribe((us: UserShout[]) => {
      let updateData = {};
      for (let s of us) {
        updateData[`${DbService.SHOUTS}/${s.$key}/user/image`] = photoName;
      }
      firebase.database().ref().update(updateData);
    });

    // change all chats
    this.db.list(`${DbService.USER_CHATS}/${userKey}`).take(1).subscribe((userChats: UserChat[]) => {
      for (let ch of userChats) {
        this.db.object(`${DbService.CHATS}/${ch.$key}/ownerId`).take(1).subscribe((ownerid) => {
          if (ownerid.$value === userKey) {
            this.db.object(`${DbService.CHATS}/${ch.$key}/image`).set(photoName);
          }
        });
      }
    });
  }

  private addToImageDeleteList(photos: ImageInfo[]) {
    console.log("Delete photos", photos);
    if (photos && photos.length > 0) {
      let deleteData = {};
      for (let p of photos) {
        deleteData[`${DbService.PICTURES_DELETED}/${p.name}`] = true;
      }
      firebase.database().ref().update(deleteData);
    }
  }



  public shrinkDatabase() {
    this.afAuth.authState.take(1).subscribe((user) => {
      if (user != null) {
        console.log(`DbService: shrinkDatabase for user called`, user);
        if (user.uid != null) {
          console.log(`DbService: shrinkDatabase for user ${user.uid} called`);
          this.shrinkShoutBlocked(user.uid);
          this.shrinkShout(user.uid);

        }
      }
    });
  }


  /* *****************************************
   Shouts
  ***************************************** */


  private shrinkShoutBlocked(userId) {
    let deleteData = {};
    let timeAgo = moment.now() - moment.duration(DbService.SHOUT_DURATION_IN_WEEKS + 1, 'weeks').asMilliseconds();

    this.db.list(`${DbService.SHOUT_BLOCK}/${userId}`, {
      query: {
        orderByChild: 'createDate',
        endAt: timeAgo
      }

    }).take(1).subscribe((blocked: ShoutBlock[]) => {
      console.log(`DbService: shrinkShoutBlocked for shouts called`);
      for (let b of blocked) {
        deleteData[`${DbService.SHOUT_BLOCK}/${userId}/${b.$key}`] = null;
      }
      firebase.database().ref().update(deleteData);
      console.log('DbService: shrinkShoutBlocked');
    });

  }

  private shrinkShout(userId) {
    let deleteData = {};
    let timeAgo = moment.now() - moment.duration(DbService.SHOUT_DURATION_IN_WEEKS + 1, 'weeks').asMilliseconds();

    this.db.list(`${DbService.USER_SHOUTS}/${userId}`, {
      query: {
        orderByChild: 'createDate',
        endAt: timeAgo
      }

    }).take(1).subscribe((userShouts: UserShout[]) => {
      console.log(`DbService: shrinkDatabase for shouts called`);
      for (let s of userShouts) {
        deleteData[`${DbService.USER_SHOUTS}/${userId}/${s.$key}`] = null;
        deleteData[`${DbService.SHOUTS}/${s.$key}`] = null;
        deleteData[`${DbService.SHOUT_LOCATIONS}/${s.$key}`] = null;
      }
      firebase.database().ref().update(deleteData);
      console.log('DbService: shrinkDatabase for shouts done');
    });

  }





  /* *****************************************
   Chats, Messages
  ***************************************** */







  /* *****************************************
   Deletes
  ***************************************** */

  public removeUser(user: User): Promise<Object> {
    console.log("DbService(RemoveUser) start", user);
    // 1. delete all chats/messages

    return this.deleteChatsForUser(user.$key).then(data => {
      console.log("DbService(RemoveUser) after deleteChatsForUser, data ", data);
      return data;
    }).then(data => {
      console.log("before deleteShoutRequestsForUser");
      return this.deleteShoutRequestsForUser(user.$key).then(shData => {
        data = Object.assign(data, shData);
        console.log("DbService(RemoveUser) after deleteShoutRequestsForUser", data);
        return data;
      });

    }).then((data) => {
      console.log("DbService(RemoveUser) before deleteShoutsForUser");
      return this.deleteShoutsForUser(user.$key).then(shData => {
        data = Object.assign(data, shData);
        console.log("DbService(RemoveUser) after deleteShoutsForUser", data);
        return data;
      });
    }).then(data => {
      data[`${DbService.USER_PICTURES}/${user.$key}`] = null;
      data[`${DbService.USER_MODIFIED}/${user.$key}`] = null;
      data[`${DbService.SHOUT_BLOCK}/${user.$key}`] = null;
      data[`${DbService.USER_BLOCK}/${user.$key}`] = null;
      data[`${DbService.USER_MESSAGES}/${user.$key}`] = null;
      data[`${DbService.USERS}/${user.$key}`] = null;
      data[`${DbService.REPORT_USER}/${user.$key}`] = null;
      console.log("DbService(RemoveUser) End", data);
      return data;
    });
  }


  private deleteChatsForUser(userKey: string): Promise<Object> {
    //    return new Promise<any>((resolve, reject) => {

    // 1. delete all chats/messages
    console.log("DbService(deleteChatsForUser) start");
    return this.db.list(`${DbService.USER_CHATS}/${userKey}`).map((userChats: UserChat[]) => {
      //        console.log(`** got user chats`, userChats);
      let promises = [];
      for (let chat of userChats) {
        //        console.log(`pushing ${userKey}, ${chat.$key}`);
        promises.push(this.leaveOrDeleteChat(userKey, chat.$key));
      }
      return Promise.all(promises).then(datas => {
        //      console.log("** Res= ", datas)
        let combinedData = {}
        for (let d of datas) {
          combinedData = Object.assign(combinedData, d);
        }
        return combinedData;
      }).then((data) => {
        console.log("DbService(deleteChatsForUser) resolve = ", data);
        //          resolve(data);
        return data;
      });
    }).take(1).toPromise();
    //    });
  }

  public leaveOrDeleteChat(userId: string, chatId: string): Promise<Object> {
    console.log("DbService(leaveOrDeleteChat) start");
    return this.db.object(`${DbService.CHATS}/${chatId}`).map((chat: Chat) => {
      return chat;
    }).take(1).toPromise().then(chat => {
      if (chat.ownerId === userId) {
        return this.deleteChat(userId, chat).then(del => {
          console.log("DbService(leaveOrDeleteChat) del", del)
          return del;
        });
      }
      else {
        return this.removeUserFromChat(userId, chat.$key, CHAT_MESSAGE_USER_LEAVED).then(del => {
          console.log("DbService(leaveOrDeleteChat) del", del)
          return del;
        });
      };
    });
  }


  public removeUserFromChat(userId: string, chatId: string, type: number = CHAT_MESSAGE_USER_DELETED): Promise<Object> {

    return this.db.object(`${DbService.CHAT_USERS}/${chatId}/${userId}`).map(uId => {
      let updateData = {};
      if ((<any>uId).$exists()) {
        updateData[`${DbService.CHAT_USERS}/${chatId}/${uId.$key}`] = null;

        var newKey = this.db.object(DbService.CHAT_MESSAGES).$ref.push().key;
        let chatMessage: ChatMessage = { type: type, createDate: firebase.database['ServerValue']['TIMESTAMP'] as number, userId: uId.$key };
        updateData[`${DbService.CHAT_MESSAGES}/${chatId}/${newKey}`] = chatMessage;
      }

      updateData[`${DbService.USER_CHATS}/${userId}/${chatId}`] = null;
      console.log("DbService(deleteChat) updateData", updateData);
      return updateData;
    }).take(1).toPromise();

  }


  public deleteChat(userId: string, chat: Chat): Promise<Object> {
    let deleteData = {};

    // 1. delete all user chats
    deleteData[`${DbService.CHAT_USERS}/${chat.$key}`] = null;
    return this.db.list(`${DbService.CHAT_USERS}/${chat.$key}`).map(userIds => {
      for (let chatUserId of userIds) {
        deleteData[`${DbService.USER_CHATS}/${chatUserId.$key}/${chat.$key}`] = null;
      }
      deleteData[`${DbService.USER_CHATS}/${userId}/${chat.$key}`] = null;

      // 2. delete chat messages
      deleteData[`${DbService.CHAT_MESSAGES}/${chat.$key}`] = null;

      // 3. delete chat
      deleteData[`${DbService.CHATS}/${chat.$key}`] = null;

      console.log("DbService(deleteChat) deleteData", deleteData);
      return deleteData;
    }).take(1).toPromise();
  }




  private deleteShoutRequestsForUser(userKey: string): Promise<Object> {
    return new Promise<Object>((resolve, reject) => {
      let deleteData = {};

      console.log("DbService(deleteShoutRequestsForUser) start")
      this.db.list(`${DbService.SHOUT_REQUEST}/${userKey}`).take(1).subscribe((shoutRequests: ShoutRequest[]) => {
        // delete all shout requests and members for user
        deleteData[`${DbService.SHOUT_REQUEST}/${userKey}`] = null;
        for (let r of shoutRequests) {
          deleteData[`${DbService.SHOUT_REQUEST_USERS}/${r.$key}`] = null;
        }


        // delete user from members in other shout requests !
        this.db.list(`${DbService.SHOUT_REQUEST_USERS}`, {
          query: {
            orderByChild: `members/${userKey}`,
            equalTo: true
          }
        }).take(1).subscribe(results => {
          console.log("Results =", results)
          for (let res of results) {
            console.log("res =", res);
            if (res.members.hasOwnProperty(`${userKey}`)) {
              deleteData[`${DbService.SHOUT_REQUEST_USERS}/${res.$key}/members/${userKey}`] = null;
            }

          }
          console.log("DbService(deleteShoutRequestsForUser) resolved =", deleteData)
          resolve(deleteData);
        });
      });
    });
  }

  private deleteShoutsForUser(userKey: string): Promise<Object> {
    //    return new Promise<Object>((resolve, reject) => {
    console.log("DbService(deleteShoutsForUser) start")
    let deleteData = {};
    return this.db.list(`${DbService.USER_SHOUTS}/${userKey}`).map((userShouts: UserShout[]) => {
      deleteData[`${DbService.USER_SHOUTS}/${userKey}`] = null;
      for (let us of userShouts) {
        deleteData[`${DbService.SHOUTS}/${us.$key}`] = null;
        deleteData[`${DbService.SHOUT_LOCATIONS}/${us.$key}`] = null;
      }
      console.log("DbService(deleteShoutsForUser) resolved =", deleteData);
      //        resolve(deleteData);
      return deleteData;
    }).take(1).toPromise();
    //    });
  }

}


