import { Injectable } from '@angular/core';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';


import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { AuthService } from './auth-service';
import { UserService } from './user-service';
import { DbService } from './db-service';
import { Settings } from './settings';
import { ChatService } from './chat-service';
import { NotificationService } from './notification-service';


import { User, Shout, UserShout, ShoutBlock, UserBlock, ShoutRequest, AppSettings } from '../models/models';

import * as moment from 'moment';
import * as GeoFire from "geofire";
import * as firebase from "firebase/app"

@Injectable()
export class ShoutService {

    geoFireRef: any;
    shoutsRef: any;

    blockedShouts: ShoutBlock[] = [];
    shoutBlockedSubscriber: Subscription = null;

    blockedUsers: UserBlock[] = [];
    userBlockedSubscriber: Subscription = null;


    // flag for reloading shouts
    needReloadShouts = false;


    constructor(afAuth: AngularFireAuth, private db: AngularFireDatabase, private authService: AuthService, private notificationService: NotificationService,
        private dbService: DbService, private userService: UserService, private chatService: ChatService, private settings: Settings) {

        this.geoFireRef = new GeoFire(this.db.list(DbService.SHOUT_LOCATIONS).$ref); // GeoFire reference 

        this.shoutsRef = firebase.database().ref(DbService.SHOUTS);

        afAuth.authState.subscribe((user: firebase.User) => {
            if (user) {
                //console.log("****** this.authService.state.uid == firebaseUser.uid", this.authService.state.uid, user.uid);
                this.shoutBlockedSubscriber = this.db.list(`${DbService.SHOUT_BLOCK}/${this.authService.currentUser.uid}`).subscribe((blocked: ShoutBlock[]) => {
                    console.log("ShoutService: subscribe blocked shouts", blocked);
                    this.blockedShouts = blocked;
                });

                this.userBlockedSubscriber = this.db.list(`${DbService.USER_BLOCK}/${this.authService.currentUser.uid}`).subscribe((userBlocked) => {
                    console.log("ShoutService: subscribe blocked users", userBlocked);
                    this.blockedUsers = userBlocked;
                });


            }
            else {
                console.log("ShoutService: unsubscribe blocked shouts");
                if (this.shoutBlockedSubscriber != null) {
                    this.shoutBlockedSubscriber.unsubscribe();
                    this.shoutBlockedSubscriber = null;
                    this.blockedShouts = [];
                }
                if (this.userBlockedSubscriber != null) {
                    this.userBlockedSubscriber.unsubscribe();
                    this.userBlockedSubscriber = null;
                    this.blockedUsers = [];
                }
            }
        }, err => {
            console.log("ShoutService: Error subscribing", err);
        });

    }

    public createShout(shoutMessage: string) {
        let user = this.userService.getUserSnapShot();
        if (user) {
            let shout: Shout = { content: shoutMessage };
            let userShout: UserShout = { content: shoutMessage };
            let shoutBlock: ShoutBlock = { type: 'own' };

            userShout.createDate = shout.createDate = shoutBlock.createDate = firebase.database['ServerValue']['TIMESTAMP'] as number;

            shout.userId = user.$key;


            shout.user = { name: user.name, birthdate: user.birthdate, gender: user.gender };
            if (user.image != null) {
                shout.user.image = user.image;
            }

            //var newKey = firebase.database().ref().child(ShoutService.SHOUTS).push().key;
            var newKey = this.db.object(DbService.SHOUTS).$ref.push().key;

            let updateData = {};
            updateData[`${DbService.SHOUTS}/${newKey}`] = shout;
            updateData[`${DbService.USER_SHOUTS}/${this.authService.currentUser.uid}/${newKey}`] = userShout;
            updateData[`${DbService.SHOUT_BLOCK}/${this.authService.currentUser.uid}/${newKey}`] = shoutBlock;


            /*
                        firebase.database().ref().update(updateData).then(() => {
                            this.authService.getPosition().then((pos) => {
                                this.geoFireRef.set(newKey, [pos.coords.latitude, pos.coords.longitude]);
                            }).catch(() => { console.error("ShoutService(createShout) can not retrieve position !") });
                        });
            */
            // create shout in db
            return this.authService.getPosition().then((pos) => {
                console.log("ShoutService(createShout) got position", pos);
                return pos;
            }).then(pos => {
                this.geoFireRef.set(newKey, [pos.coords.latitude, pos.coords.longitude]);
                return firebase.database().ref().update(updateData);
            }).catch(err => {
                console.error("ShoutService(createShout) can not create Shout!", err);
                throw err;
            });


        }
    }

    public getShout(key: string): Observable<Shout> {
        return this.db.object(`${DbService.SHOUTS}/${key}`);
    }

    public deleteShout(key: string) {
        let updateData = {};
        updateData[`${DbService.USER_SHOUTS}/${this.authService.currentUser.uid}/${key}`] = null;
        updateData[`${DbService.SHOUT_BLOCK}/${this.authService.currentUser.uid}/${key}`] = null;
        updateData[`${DbService.SHOUT_LOCATIONS}/${key}`] = null;
        return firebase.database().ref().update(updateData);
        /*
                this.db.object(`${DbService.USER_SHOUTS}/${this.authService.state.uid}/${key}`).remove();
                this.db.object(`${DbService.SHOUT_BLOCK}/${this.authService.state.uid}/${key}`).remove();
                this.db.object(`${DbService.SHOUTS}/${key}`).remove();
                this.geoFireRef.remove(key);
        */
    }



    public getUserShouts(count: number = DbService.MAX_SHOUTS): FirebaseListObservable<UserShout[]> {
        let o: FirebaseListObservable<any>;
        if (this.authService.authenticated) {
            o = this.db.list(`${DbService.USER_SHOUTS}/${this.authService.currentUser.uid}`, {
                query: {
                    limitToLast: count
                }
            });
        }
        return o;
    }

    public getRecentUserShouts(): FirebaseListObservable<UserShout[]> {

        var startdate = moment.now();
        let timeAgo = startdate - moment.duration(DbService.SHOUT_DURATION_IN_WEEKS, 'weeks').asMilliseconds();

        let o: FirebaseListObservable<any>;
        if (this.authService.authenticated) {
            o = this.db.list(`${DbService.USER_SHOUTS}/${this.authService.currentUser.uid}`, {
                query: {
                    orderByChild: 'createDate',
                    startAt: timeAgo,
                    limitToLast: 3
                }
            });
        }
        return o;
    }


    public getShoutsNearbyAlt(): Observable<Shout[]> {

        console.log("ShoutService(getShoutsNearby): Load Shouts nearby");
        return Observable.create(o => {
            let shouts: Shout[] = [];
            return this.settings.load().then(() => {
                console.log("ShoutService: Settings loaded", this.settings.allSettings)
                return this.settings.allSettings;
            }).then((settings: AppSettings) => {
                console.log("ShoutService: got Settings loaded", settings)
                return this.authService.getPosition();
            }).then(pos => {
                console.log("ShoutService: got location", pos);
                let settings = this.settings.allSettings
                let currentLocation = [pos.coords.latitude, pos.coords.longitude];
                let geoQuery = this.geoFireRef.query({
                    center: currentLocation,
                    radius: settings.searchRange
                });
                geoQuery.on("ready", () => {
                    // cancel geoQuery after loading initial data, so no reload occures !!!
                    console.log("ShoutService(getShoutsNearby), readycalled => cancel geoQuery!");
                    geoQuery.cancel();
                    o.next(shouts);
                });

                geoQuery.on("key_entered", (key, location, distance) => {
                    this.filterShout(settings, key).subscribe((shout: Shout) => {
                        if (shout != null) {
                            shout.distance = distance;
                            console.log(`ShoutService(getShoutsNearby): add key ${key}, distance = ${shout.distance}`);

                            shouts.push(shout);
                        }
                    });
                });
                return function () {
                    geoQuery.cancel();
                    console.log('ShoutService(getShoutsNearby): observable disposed');
                };
            }).catch(error => {
                console.error("ShoutService(getShoutsNearby): Error", error);
            });

        });
    }





    public getShoutsNearby(): Observable<Shout | string> {

        console.log("ShoutService(getShoutsNearby): Load Shouts nearby");
        return Observable.create(o => {
            return this.settings.load().then(() => {
                console.log("ShoutService: Settings loaded", this.settings.allSettings)
                return this.settings.allSettings;
            }).then((settings: AppSettings) => {
                console.log("ShoutService: got Settings loaded", settings)
                return this.authService.getPosition();
            }).then(pos => {
                console.log("ShoutService: got location", pos);
                let settings = this.settings.allSettings
                let currentLocation = [pos.coords.latitude, pos.coords.longitude];
                let geoQuery = this.geoFireRef.query({
                    center: currentLocation,
                    radius: settings.searchRange
                });
                geoQuery.on("ready", () => {
                    // cancel geoQuery after loading initial data, so no reload occures !!!
                    console.log("ShoutService(getShoutsNearby), readycalled => cancel geoQuery!");
                    geoQuery.cancel();
                });

                geoQuery.on("key_entered", (key, location, distance) => {
                    this.filterShout(settings, key).subscribe((shout: Shout) => {
                        if (shout != null) {
                            shout.distance = distance;
                            console.log(`ShoutService(getShoutsNearby): add key ${key}, distance = ${shout.distance}`);

                            o.next(shout);
                        }
                    });
                });

                geoQuery.on("key_exited", (key, location, distance) => {
                    console.log(`ShoutService(getShoutsNearby): remove key ${key}`);
                    o.next(key);
                });

                return function () {
                    geoQuery.cancel();
                    console.log('ShoutService(getShoutsNearby): observable disposed');
                };
            }).catch(error => {
                console.error("ShoutService(getShoutsNearby): Error", error);
                o.error(error);
            });

        });
    }

    public voteShout(shoutSnap: Shout, like: boolean) {
        // first, check if shout exists
        this.db.object(`${DbService.SHOUTS}/${shoutSnap.$key}`).take(1).subscribe((shout: Shout) => {
            if ((<any>shout).$exists()) {
                let shoutBlock: ShoutBlock = { type: like ? 'yes' : 'no' };
                shoutBlock.createDate = firebase.database['ServerValue']['TIMESTAMP'] as number;
                let updateData = {};
                updateData[`${DbService.SHOUT_BLOCK}/${this.authService.currentUser.uid}/${shout.$key}`] = shoutBlock;

                // add shout-chat
                if (like) {
                    this.createShoutRequest(shout);
                }

                firebase.database().ref().update(updateData);
            }
            else {
                console.log("ShoutService(voteShout) Cant create ShoutRequest because origShout doesn't exists any more")
            }
        });
    }

    private createShoutRequest(shout: Shout) {
        let me = this.userService.getUserSnapShot();
        if (me) {

            let reqRef = this.db.object(`${DbService.SHOUT_REQUEST}/${shout.userId}/${shout.$key}`);

            // do we have an shout request? take(1) gets one object and discards subscription !!
            reqRef.take(1).subscribe((req: ShoutRequest) => {
                // update, add new User
                if ((<any>req).$exists()) {
                    req.isNew = true;
                }
                // create new ShoutRequest
                else {
                    req = { subject: shout.content, isNew: true, createDate: firebase.database['ServerValue']['TIMESTAMP'] as number };

                }
                reqRef.set(req);

                this.db.object(`${DbService.SHOUT_REQUEST_USERS}/${shout.$key}/members/${me.$key}`).set(true)

                this.userService.updateUserMessagesUnread([shout.userId]);

                this.userService.getUserByKey(shout.userId).take(1).subscribe((user: User) => {
                    this.notificationService.shoutRequestNotification(me, user, shout);
                });
            });
        }
    }

    public markShoutRequest(shoutRequest: ShoutRequest): firebase.Promise<void> {
        console.log(`set shoutRequest read to false for ${this.authService.currentUser.uid}/${shoutRequest.$key}`)

        return this.db.object(`${DbService.SHOUT_REQUEST}/${this.authService.currentUser.uid}/${shoutRequest.$key}/isNew`).set(false);

        // .then(()=>{
        //     return this.dbService.updateMessagesCounter(this.authService.state.uid, false);
        // });
    }

    public getShoutRequestsAndUsers(): Observable<ShoutRequest[]> {
        return this.getShoutRequests().map((requests: ShoutRequest[]) => {

            for (let r of requests) {
                r.users = [];
                // this.getShoutRequestUserKeys(r.$key).take(1).subscribe(keys => {
                let sub = this.getShoutRequestUserKeys(r.$key).subscribe(keys => {
                    r.users = keys;
                }, err => { }, () => {
                    console.log("Completed!, unsubscribe ");
                    sub.unsubscribe();
                });
            }
            console.log("Finished = ", requests)
            return requests;


        })
    }

    private getShoutRequests(): FirebaseListObservable<ShoutRequest[]> {
        return this.db.list(`${DbService.SHOUT_REQUEST}/${this.authService.currentUser.uid}`);
    }

    private getShoutRequestUserKeys(requestKey: string): Observable<string[]> {
        return this.db.list(`${DbService.SHOUT_REQUEST_USERS}/${requestKey}/members`).map((users => {
            let str = [];
            for (let u of users) {
                str.push(u.$key);
            }
            return str;
        }));
    }

    public rejectShoutRequest(request: ShoutRequest, user: User) {
        console.log(`ShoutService: Reject User ${user.$key}`);

        this.removeUserFromShoutRequest(request.$key, user.$key).then(() => {
            console.log(`ShoutService: removeUserFromShoutRequest finished}`);
        });
    }

    public acceptShoutRequest(request: ShoutRequest, user: User) {
        console.log("ShoutService: Accept Shout Request " + user.$key);
        this.removeUserFromShoutRequest(request.$key, user.$key).then(() => {
            let me = this.userService.getUserSnapShot();
            if (me) {
                this.chatService.createChat(me, user, request);

                this.notificationService.acceptShoutRequestNotification(me, user, request);
            }
        });
    }

    public deleteShoutRequest(request: ShoutRequest) {
        console.log("deleteShoutRequest start");
        let updateData = {};
        updateData[`${DbService.SHOUT_REQUEST}/${this.authService.currentUser.uid}/${request.$key}`] = null;
        updateData[`${DbService.SHOUT_REQUEST_USERS}/${request.$key}`] = null;
        return firebase.database().ref().update(updateData).then(() => {
            console.log("deleteShoutRequest done");
        });
    }

    private removeUserFromShoutRequest(shoutRequestKey: string, userKey: string): Promise<any> {
        let updateData = {};
        updateData[`${DbService.SHOUT_REQUEST_USERS}/${shoutRequestKey}/members/${userKey}`] = null;

        // do we have other members ?
        console.log("removeUserFromShoutRequest start");

        return this.db.list(`${DbService.SHOUT_REQUEST_USERS}/${shoutRequestKey}/members`).map(mbrs => {
            console.log("removeUserFromShoutRequest: Mbrs=", mbrs);
            if (mbrs.length == 1 && mbrs[0].$key === userKey) {
                updateData[`${DbService.SHOUT_REQUEST}/${this.authService.currentUser.uid}/${shoutRequestKey}`] = null;
            }
            console.log("removeUserFromShoutRequest: updateData=", updateData);
            return firebase.database().ref().update(updateData).then(() => {
                console.log("removeUserFromShoutRequest done");
            });


        }).take(1).toPromise()
    }


    private filterShout(settings: AppSettings, key: string): Observable<Shout> {

        return this.getShout(key).take(1).filter((shout: Shout) => {
            let ok: boolean = false;
            console.log("Filter Shout", shout);

            // check if shout is blocked
            if (this.blockedShouts.find(((shout: ShoutBlock) => {
                // console.log(`test shout ${shout.$key} != ${key} = ${shout.$key !== key}`);
                return shout.$key === key;
            })) != null) return false;

            // check if user is blocked
            if (this.blockedUsers.find(((u: UserBlock) => {
                // console.log(`test user ${u.$key} != ${shout.userId} = ${u.$key !== shout.userId}`);
                return u.$key === shout.userId;
            })) != null) return false;

            // test age
            let age = -100;
            let birthdate = moment(shout.user.birthdate, ["MM/DD/YYYY", "MM/DD"]);
            if (birthdate.isValid()) {
                age = moment().diff(birthdate, 'years');
            }
            let ageRangeUpper = settings.ageRange.upper < 55 ? settings.ageRange.upper : 99;
            ok = (age >= settings.ageRange.lower && age <= ageRangeUpper);
            console.log("Verify Age", ok)

            if (ok) {
                ok = settings.show === "a" || (settings.show === "m" && shout.user.gender === 'male') || (settings.show === "f" && shout.user.gender === 'female');
            }
            console.log("OK", ok)

            return ok;
        });
    }


}
