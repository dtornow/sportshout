// import { User } from './user';
// import { Api } from './api';
import { Settings } from './settings';
import {AuthService} from './auth-service';
import {UserService} from './user-service';
import {ShoutService} from './shout-service';
import {ChatService} from './chat-service';
import {ImageService} from './image-service';
import {NotificationService} from './notification-service';
import {DbService} from './db-service';
// import { Items } from '../mocks/providers/items';

export {
//   User,
//   Api,
  Settings,
  AuthService,
  UserService,
  ShoutService,
  ChatService,
  ImageService,
  NotificationService,
  DbService
};
