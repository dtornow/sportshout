import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';

import { OneSignal, OSNotification } from '@ionic-native/onesignal';
import { Badge } from '@ionic-native/badge';
import { DbService } from './db-service';

import { User, Shout, ShoutRequest, Chat } from '../models/models';

// import * as firebase from "firebase/app"

import { environment } from '../environments/environment';


@Injectable()
export class NotificationService {


  public static ONESIGNAL_APP_ID = environment.onesignal.appId;
  public static ONESIGNAL_GOOGLE_PROJECT_ID = environment.onesignal.googleProjectId;

  public static ONESIGNAL_ANDROID_ACCENT_COLOR = 'FF3F51B5';
  public static ONESIGNAL_ANDROID_GRP_SHOUTREQUESTS = 'ShoutRequests';
  public static ONESIGNAL_ANDROID_GRP_ACCEPTSHOUTREQUESTS = 'AcceptShoutRequests';
  public static ONESIGNAL_ANDROID_GRP_MESSAGES = 'Messages';

  private readonly updateAppBadgeTypes = ['shoutRequest', 'acceptRequest', 'message'];



  private notificationReceivedObserver: any;
  public notificationReceived: Observable<any>;

  constructor(private db: AngularFireDatabase, private oneSignal: OneSignal, private platform: Platform, private badge: Badge) {
    platform.ready().then(() => {
      console.log("NotificationService: Initialize OneSignal");
      if (platform.is('cordova')) {
        this.initialize();
      }

      this.notificationReceivedObserver = null;
      this.notificationReceived = Observable.create(observer => {
        this.notificationReceivedObserver = observer;
      })
    });
  }

  public getApplicationIds() {
    return this.oneSignal.getIds();
  }

  public shoutRequestNotification(me: User, user: User, shout: Shout) {
    console.log("NotificationService(shoutRequestNotification) shout ", shout)

    let notificationObj = {
      isAppInFocus: true, shown: true,
      headings: { en: `You got one new Request from ${me.name}`, de: `Eine neue Anfrage von ${me.name}` },
      contents: { en: `@"${shout.content}"`, de: `@"${shout.content}"` },
      android_accent_color: NotificationService.ONESIGNAL_ANDROID_ACCENT_COLOR,
      android_group: NotificationService.ONESIGNAL_ANDROID_GRP_SHOUTREQUESTS,
      include_player_ids: [user.osUserId],
      data: { 'type': 'shoutRequest', 'id': shout.$key }
    };

    this.oneSignal.postNotification(<OSNotification>notificationObj).then(result => {
      console.log("NotificationService(shoutRequestNotification) postNotification Result", result)
    }).catch(err => {
      console.error("NotificationService: Error in sending notification", err);
    });
  }

  public acceptShoutRequestNotification(me: User, user: User, request: ShoutRequest) {
    console.log("NotificationService(acceptShoutRequestNotification) shout ", request)

    let notificationObj = {
      isAppInFocus: true, shown: true,
      headings: { en: `${me.name} accepted your request`, de: `${me.name} hat deine Anfrage angenommen` },
      contents: { en: `${request.subject}`, de: `${request.subject}` },
      android_accent_color: NotificationService.ONESIGNAL_ANDROID_ACCENT_COLOR,
      android_group: NotificationService.ONESIGNAL_ANDROID_GRP_ACCEPTSHOUTREQUESTS,
      include_player_ids: [user.osUserId],
      data: { 'type': 'acceptRequest', 'id': request.$key }
    };

    this.oneSignal.postNotification(<OSNotification>notificationObj).then(result => {
      console.log("NotificationService(acceptShoutRequestNotification) postNotification Result", result)
    }).catch(err => {
      console.error("NotificationService: Error in sending notification", err);
    });
  }


  public chatMessageNotification(chat: Chat, message: string, users: string[], sender: User) {
    console.log("NotificationService(chatMessageNotification) users ", users);

    let msg = message.length > 40 ? message.substr(0, 37) + "..." : message;
    let notificationObj = {
      isAppInFocus: true, shown: true,
      headings: { en: `${sender.name}@${chat.subject}`, de: `${sender.name}@${chat.subject}` },
      contents: { en: `${msg}`, de: `${msg}` },
      android_accent_color: NotificationService.ONESIGNAL_ANDROID_ACCENT_COLOR,
      android_group: NotificationService.ONESIGNAL_ANDROID_GRP_MESSAGES,
      include_player_ids: [],
      data: { 'type': 'message', 'chatId': chat.$key }
    };

    // get all the user objects
    let usr = [];
    for (let u of users) {
      usr.push(this.db.object(`${DbService.USERS}/${u}`).take(1));
    }

    Observable.forkJoin(usr).subscribe((firebaseUsers: User[]) => {
      for (let u of firebaseUsers) {
        notificationObj.include_player_ids.push(u.osUserId);
      }


      console.log("notificationObj", notificationObj)

      // send notification
      this.oneSignal.postNotification(<OSNotification>notificationObj).then(result => {
        console.log("NotificationService(chatMessageNotification) postNotification Result", result)
      }).catch(err => {
        console.error("NotificationService: Error in sending notification", err);
      });
    }, err => { }, () => { console.log("completed") });

    /*
        for (let u of users) {
          this.db.object(`${DbService.USERS}/${u}`).take(1).subscribe((user: User) => {
            notificationObj.include_player_ids = [user.osUserId];
    
            // send notification
            this.oneSignal.postNotification(<OSNotification>notificationObj).then(result => {
              console.log("NotificationService(chatMessageNotification) postNotification Result", result)
            }).catch(err => {
              console.error("NotificationService: Error in sending notification", err);
            });
          })
        }
    */


    /*
        for(let u of users) {
          this.db.list(`${DbService.USERS}/${u}`).take(1).subscribe((user:User)=>{
            user.osUserId;
          });
    
        }
    
    
        let notificationObj = {
          isAppInFocus: true, shown: true,
          headings: { en: `You got one new Message from `, de: `Neue Nachricht von ???` },
          contents: { en: `auf deinen Shout "${shout.content}"`, de: `auf deinen Shout "${shout.content}"` },
          android_accent_color: NotificationService.ONESIGNAL_ANDROID_ACCENT_COLOR,
          android_group: NotificationService.ONESIGNAL_ANDROID_GRP_SHOUTREQUESTS,
          include_player_ids: [user.osUserId]
        };
        */
  }



  public setSubscription(subscription: boolean) {
    this.oneSignal.setSubscription(subscription);
  }

  public clearAppBadge() {
    this.badge.clear();
  }

  private async updateAppBadge(nType: string) {
    console.log(`NotificationService: update Badge with type =${nType}`);
    try {
      let i = this.updateAppBadgeTypes.findIndex(s => { return s === nType });
      if (i > -1) {
        console.log("NotificationService: incerease AppBadge");
        let number = await this.badge.increase(1);
        console.log(`NotificationService: updateAppBadge succeeded, Count=${number}`)
      }
    } catch (e) {
      console.error(e);
    }
  }

  async requestBadgePermission() {
    try {
      let hasPermission = await this.badge.hasPermission();
      console.log(`NotificationService: requestBadgePermission hasPermission =${hasPermission}`);
      if (!hasPermission) {
        let permission = await this.badge.registerPermission();
        console.log(`NotificationService: requestBadgePermission permission =${permission}`);
      }
    } catch (e) {
      console.error(e);
    }
  }


  private initialize() {
    if (this.platform.is('cordova')) {
      this.oneSignal.startInit(NotificationService.ONESIGNAL_APP_ID, NotificationService.ONESIGNAL_GOOGLE_PROJECT_ID);
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification); // change this later to none !
      this.oneSignal.setSubscription(true);
      this.oneSignal.iOSSettings({ kOSSettingsKeyAutoPrompt: true, kOSSettingsKeyInAppLaunchURL: false });

      this.oneSignal.handleNotificationReceived().subscribe((value) => {
        console.log("NotificationService: handleNotificationReceived ==> ", value);

        // update badges
        let additionalData = value.payload.additionalData;
        if (additionalData) {
          this.updateAppBadge(additionalData.type)
        }
      });

      this.oneSignal.handleNotificationOpened().subscribe((value) => {
        console.log("NotificationService: handleNotificationOpened ==> ", value);
        let additionalData = value.notification.payload.additionalData;
        if (additionalData) {
          if (this.notificationReceivedObserver) {
            this.notificationReceivedObserver.next(additionalData);
          }
        }

      });
      this.oneSignal.endInit();


      // request badge permission
      this.requestBadgePermission();

    }

  }
}
