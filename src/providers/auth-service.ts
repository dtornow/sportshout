import { Injectable } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';

import { Platform } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';

import { UserData } from '../models/models';

import { UserService } from './user-service'
import { NotificationService } from './notification-service'

import * as firebase from 'firebase/app';
// import * as moment from 'moment';

@Injectable()
export class AuthService {
  private _currentUser: firebase.User;
  private _geoposition: Geoposition;

  // private userSubscriber: Subscription = null;
  // private user: User;

  private registerActive = false;

  constructor(private afAuth: AngularFireAuth, private platform: Platform,
    private notificationService: NotificationService,
    private userService: UserService,
    private geolocation: Geolocation, private facebook: Facebook) {

    afAuth.authState.subscribe((user: firebase.User) => {
      console.log("AuthService: Auth State changed = ", user)
      this._currentUser = user;
    });

    platform.resume.subscribe(() => {
      console.log("AuthService: onresume called");
     // this.reloadPosition();
    });

    // platform.ready().then(() => {
    //   // pre-warm position
    //   console.log("AuthService: Try to get position");
    //   this.getPosition().then(() => {
    //     console.log("AuthService: Get position", this._geoposition);
    //   }).catch(err=>{
    //     console.log("AuthService: Get position failed, reason ", err);
    //   });
    //}); 
  }

  get isLoginActive() {
    return this.registerActive;
  }

  // dont use cache here !!!
  public getPosition(): Promise<Geoposition> {
    /*
    console.log("AuthService:(getPosition) try to get position");
      let locationOptions = { maximumAge: (1000 * 60 * 20), timeout: 10000, enableHighAccuracy: false };
      return this.geolocation.getCurrentPosition(locationOptions).then((resp) => {
        console.log('AuthService:(getPosition): got position', resp);
        this._geoposition = resp;
        return this._geoposition;
      }).catch((error) => {
        console.log('AuthService:(getPosition): Error getting location, setting to default munich !', error);
        this._geoposition = <any>{ coords: { latitude: 48.0674098, longitude: 11.1419438, accuracy: 30 }, timestamp: new Date() };
        //reject(new Error(error));
        return this._geoposition;
      });
*/

 console.log("AuthService:(getPosition) try to get position");
      let locationOptions = { maximumAge: (1000 * 60 * 20), timeout: 10000, enableHighAccuracy: false };
      return this.geolocation.getCurrentPosition(locationOptions).then((resp) => {
        console.log('AuthService:(getPosition): got position', resp);
        this._geoposition = resp;
        return this._geoposition;
      });




  }

  // private reloadPosition() {
  //   console.log("AuthService:(reloadPosition) try to get position");
  //   let locationOptions = { timeout: 10000, enableHighAccuracy: false };
  //   return this.geolocation.getCurrentPosition(locationOptions).then((resp) => {
  //     console.log('AuthService:(reloadPosition): got position', resp);
  //     this._geoposition = resp;
  //     return this._geoposition;
  //   }).catch(error => {
  //     console.log('AuthService:(reloadPosition): Error getting location!', error);
  //     this._geoposition = <any>{ coords: { latitude: 48.0674098, longitude: 11.1419438, accuracy: 30 }, timestamp: new Date() };
  //     return this._geoposition;
  //   });
  // }


  public loginWithFacebook(): firebase.Promise<any> {
    this.registerActive = true;

    if (this.platform.is('cordova')) {
      console.log("loginWithFacebook: native login using cordova");

      return this.facebook.login(['public_profile', 'email', 'user_birthday']).then(facebookData => {
        let provider = firebase.auth.FacebookAuthProvider.credential(facebookData.authResponse.accessToken);
        return provider;

      }).then(provider => {
        return firebase.auth().signInWithCredential(provider).then(firebaseData => {
          console.log("loginWithFacebook: login with facebook plugin succeeded, got data = ", firebaseData);

          let userdata: UserData = {
            uid: firebaseData.uid,
            name: firebaseData.displayName,
            email: firebaseData.email,
            provider: 'facebook.com',
            image: firebaseData.image
          };
          return userdata;
        })
      }).then((userdata: UserData) => {
        return this.facebook.api('me/?fields=first_name,gender,birthday,picture.type(large)', ["public_profile", "user_birthday"]).then((result) => {
          console.log("loginWithFacebook: Facebook API returned = ", result);
          userdata.gender = result.gender;
          userdata.name = result.first_name;
          userdata.birthdate = result.birthday;

          if (result.picture.data.is_silhouette) {
            userdata.image = null;
          }
          else {
            userdata.image = result.picture.data.url;
          }
          return userdata;
        })
      }).then((userdata: UserData) => {
        return this.notificationService.getApplicationIds().then(ids => {
          console.log("loginWithFacebook: got getApplicationIds", ids);
          userdata.osUserId = ids.userId;
          userdata.osPushToken = ids.pushToken;
          return userdata;
        })
      }).then((userdata: UserData) => {
        return this.userService.createUserIfNotExist(userdata.uid, userdata);
      }).catch(error => {
        console.log("loginWithFacebook: Error", error);
        this.registerActive = false;
        throw error;
      }).then(_ => {
        console.log("loginWithFacebook: unset registeractive ", false);
        this.registerActive = false;
      });

    } else {
      console.log("loginWithFacebook: web login");
      var provider = new firebase.auth.FacebookAuthProvider();
      return this.afAuth.auth.signInWithPopup(provider).then((facebookData) => {
        console.log("loginWithFacebook: firebase login succeeded, data = ", facebookData);

        var token = facebookData.credential.accessToken;


        let userdata: UserData = {
          uid: facebookData.user.uid,
          name: facebookData.user.displayName,
          email: facebookData.user.email,
          provider: 'facebook.com',
          //          image: facebookData.user.photoURL
          image: null
        };
        return userdata;
      }).then((userdata: UserData) => {
        return this.userService.createUserIfNotExist(userdata.uid, userdata);
      }).catch(error => {
        console.log("loginWithFacebook: Error", error);
        this.registerActive = false;
      }).then(_ => {
        console.log("loginWithFacebook: unset registeractive ", false);
        this.registerActive = false;
      });
    };
  }



  get currentUser(): firebase.User {
    return this._currentUser;
  }

  get authenticated(): boolean {
    return this._currentUser !== null;
  }

  signOut(): void {
    // if (this.userSubscriber) {
    //   this.userSubscriber.unsubscribe();
    // }

    this.afAuth.auth.signOut();
  }


}