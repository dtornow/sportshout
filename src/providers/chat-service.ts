import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';


import { AuthService } from './auth-service';
import { UserService } from './user-service';
import { DbService } from './db-service';
import { NotificationService } from './notification-service';
import { User, ShoutRequest, Chat, UserChat, ChatMessage } from '../models/models';
import { CHAT_MESSAGE_TEXT } from '../models/models';

import * as firebase from "firebase/app"
// import * as moment from 'moment';

@Injectable()
export class ChatService {

    constructor(private db: AngularFireDatabase, private userService: UserService,
        private authService: AuthService, private dbService: DbService, private notificationService: NotificationService) {

    }


    public getUserChats(): Observable<UserChat[]> {
        return this.db.list(`${DbService.USER_CHATS}/${this.authService.currentUser.uid}`, {
            query: {
                orderByChild: 'updateDate'
            }
        }).map((chats) => {
            return chats.map(c => {
                c.chat = this.db.object(`${DbService.CHATS}/${c.$key}`);
                return c;
            })
            // order by updateDate
        }).map(chats => {
            console.log("getUserChats order", chats)
            return chats.reverse()
        });
        //});
    }
    /*
    
        public createChat(request: ShoutRequest, user: User): void {
            console.log(`ChatService: createChat`)
            let me = this.userService.getUserSnapShot();
    
            let chatKey = request.$key;
    
            let chatRef = this.db.object(`${DbService.CHATS}/${chatKey}`);
            chatRef.take(1).subscribe((chat: Chat) => {
                // update, add new User
                if ((<any>chat).$exists()) {
                    chat.users = chat.users || [];
                    if (chat.users.indexOf(user.$key) < 0) {
                        chat.users.push(user.$key);
                        chatRef.set(chat);
                    }
                }
                // create new Chat
                else {
                    let chat: Chat = { subject: request.subject, ownerId: me.$key, name: me.name, users: [user.$key] };
                    if (me.image != null) {
                        chat.image = me.image;
                    }
                    chat.createDate = chat.updateDate = firebase.database['ServerValue']['TIMESTAMP'] as number;
                    chatRef.set(chat);
                }
    
    
                // now add the user chats !
                let userChat: UserChat = { unreadMessages: 0, updateDate: firebase.database['ServerValue']['TIMESTAMP'] as number };
                let updateData = {};
    
                updateData[`${DbService.CHAT_USERS}/${chatKey}/${user.$key}`] = true;
    
                updateData[`${DbService.USER_CHATS}/${user.$key}/${chatKey}`] = userChat;
                updateData[`${DbService.USER_CHATS}/${me.$key}/${chatKey}`] = userChat;
                firebase.database().ref().update(updateData);
            });
    
        }
    */

    public createChat(me: User,  user: User, request: ShoutRequest): void {
        console.log(`ChatService: createChat`)

        let chatKey = request.$key;

        let chatRef = this.db.object(`${DbService.CHATS}/${chatKey}`);
        chatRef.take(1).subscribe((chat: Chat) => {
            let updateData = {};

            // update, add new User
            let chatExists = (<any>chat).$exists();
            console.log(`ChatService: chat exists = ${chatExists}`);
            if (!chatExists) {
                let chat: Chat = { subject: request.subject, ownerId: me.$key, name: me.name /*, users: [user.$key] */ };
                if (me.image != null) {
                    chat.image = me.image;
                }
                chat.createDate = chat.updateDate = firebase.database['ServerValue']['TIMESTAMP'] as number;
                updateData[`${DbService.CHATS}/${chatKey}`] = chat;
            }

            updateData[`${DbService.CHAT_USERS}/${chatKey}/${user.$key}`] = true;

            // now add the user chats !
            let updateDate = firebase.database['ServerValue']['TIMESTAMP'] as number
            let userChat: UserChat = { unreadMessages: 0, updateDate: updateDate };
            updateData[`${DbService.USER_CHATS}/${user.$key}/${chatKey}`] = userChat;

            if (chatExists) {
                updateData[`${DbService.USER_CHATS}/${me.$key}/${chatKey}/updateDate`] = updateDate;
            }
            else {
                updateData[`${DbService.USER_CHATS}/${me.$key}/${chatKey}`] = userChat;
            }
            console.log("ChatService: UpdateDaza = ", updateData);
            firebase.database().ref().update(updateData);
        });

    }


    public markUserChatAsRead(chatKey: string) {
        this.userService.getUser().take(1).subscribe((user: User) => {
            this.db.object(`${DbService.USER_CHATS}/${user.$key}/${chatKey}/unreadMessages`).set(0);
        });

        /*
                return this.userService.getUserOld().then((me) => {
                    return this.af.database.object(`${DbService.USER_CHATS}/${me.$key}/${chatKey}/unreadMessages`).set(0);
                });
                */
    }


    public createChatMessage(chatId: string, message: string): firebase.Promise<any> {
        let ownUserId = this.authService.currentUser.uid;
        let time = firebase.database['ServerValue']['TIMESTAMP'] as number;
        var newKey = this.db.object(DbService.CHAT_MESSAGES).$ref.push().key;

        let updateData = {};
        let chatMessage: ChatMessage = { type: CHAT_MESSAGE_TEXT, createDate: time, message: message, userId: ownUserId };
        updateData[`${DbService.CHATS}/${chatId}/updateDate`] = time;
        updateData[`${DbService.CHAT_MESSAGES}/${chatId}/${newKey}`] = chatMessage;


        this.db.object(`${DbService.CHATS}/${chatId}`).take(1).map((chat: Chat) => {
            return chat;
        }).subscribe((chat: Chat) => {
            this.db.list(`${DbService.CHAT_USERS}/${chatId}`).take(1).subscribe(userIds => {
                let users = [chat.ownerId];
                for (let userId of userIds) {
                    if (users.indexOf(userId.$key) === -1) {
                        users.push(userId.$key);
                    }
                }

                this.informUsersOfNewMessage(chat, message, users.filter((userId) => {
                    return userId !== ownUserId;
                }));
            });


        })




        /*
                this.db.object(`${DbService.CHATS}/${chatId}`).take(1).map((chat: Chat) => {
                    chat.users = chat.users || [];
                    //            console.log("got chat users :", chat.users);
                    chat.users.push(chat.ownerId);
        
                    //            console.log("after push owner id => chat users :", chat.users);
                    return chat.users;
                }).subscribe((users: string[]) => {
                    //         console.log("inform chat users :", users);
                    // inform all users exept me
                    this.informUsersOfNewMessage(chatId, message, users.filter((userId) => {
                        return userId !== ownUserId;
                    }));
                });
        */

        return firebase.database().ref().update(updateData);
    }

    public getChat(chatId: string): Observable<Chat> {
        return this.db.object(`${DbService.CHATS}/${chatId}`)
    }

    public getChatUsers(chatId: string) {
        return this.db.list(`${DbService.CHAT_USERS}/${chatId}`);
    }

    public getChatMessages(chatId: string): Observable<ChatMessage[]> {
        let userCache = new Map<string, User>();

        let o: FirebaseListObservable<ChatMessage[]>;
        if (this.authService.authenticated) {
            return this.db.list(`${DbService.CHAT_MESSAGES}/${chatId}`).map((messages: ChatMessage[]) => {
                for (let m of messages) {
                    let u = userCache.get(m.userId);
                    if (u == null) {
                        this.userService.getUserByKey(m.userId).take(1).subscribe(user => {
                            m.user = (<any>user).$exists() ? user : { $key: null, name: '-'};
                            userCache.set(user.$key, user);
                        });

                    }
                    else {
                        m.user = u;
                    }
                }
                return messages;
            });
        }
        return o;
    }



    public leaveOrDeleteChat(chatId: string) {
        return this.dbService.leaveOrDeleteChat(this.authService.currentUser.uid, chatId).then((deleteData) => {
            console.log("ChatService(leaveOrDeleteChat) deleteData", deleteData);
            return firebase.database().ref().update(deleteData);
        });
    }

    public removeUserFromChat(chatId: string, userId: string) {
        console.log("ChatService(removeUserFromChat) chatId", chatId);
        return this.dbService.removeUserFromChat(userId, chatId).then((deleteData) => {
            console.log("ChatService(removeUserFromChat) deleteData", deleteData);
            return firebase.database().ref().update(deleteData);
        });
    }

    private informUsersOfNewMessage(chat: Chat, message: string, users: string[]) {
        console.log("informUsersOfNewMessage got usres ", users)
        let updateDate = firebase.database['ServerValue']['TIMESTAMP'] as number;
        for (let userId of users) {
            this.db.object(`${DbService.USER_CHATS}/${userId}/${chat.$key}`).$ref.transaction((userChat: UserChat) => {
                if (userChat != null) {
                    userChat.updateDate = updateDate;
                    userChat.unreadMessages = userChat.unreadMessages ? userChat.unreadMessages + 1 : 1
                    return userChat;
                }
                else {
                    return { unreadMessages: 1, updateDate: updateDate };
                }
            });

        }
        // set message unread flags !
        this.userService.updateUserMessagesUnread(users);

        this.notificationService.chatMessageNotification(chat, message, users, this.userService.getUserSnapShot());
    }

}