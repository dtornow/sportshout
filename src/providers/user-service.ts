import { Injectable } from '@angular/core';

import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';

import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { User, UserData, ImageInfo } from '../models/models';

import { ImageService } from './image-service'
import { DbService } from './db-service'

import * as firebase from 'firebase/app';
import * as moment from 'moment';


@Injectable()
export class UserService {

    // private userSubscriber: Subscription = null;
    private user: User;
    private _user: BehaviorSubject<User>;

    constructor(afAuth: AngularFireAuth, private db: AngularFireDatabase,
        private imageService: ImageService, private dbService: DbService) {

        this._user = <BehaviorSubject<User>>new BehaviorSubject(null);

        let userSubscriber: Subscription = null;
        afAuth.authState.subscribe((fbUser: firebase.User) => {
            if (fbUser) {
                userSubscriber = this.db.object(`${DbService.USERS}/${fbUser.uid}`).subscribe((user: User) => {
                    if ((<any>user).$exists()) {
                        this.user = user;
                        console.log("** got user = ", user)
                    }
                    else {
                        this.user = null;
                    }
                    this._user.next(this.user);
                });
            }
            else {
                console.log("UserService: unsubscribe user");
                if (userSubscriber != null) {
                    userSubscriber.unsubscribe();
                    userSubscriber = null;
                    this.user = null;
                    this._user.next(this.user);
                }
            }
        }, err => {
            console.log("UserService: Error subscribing", err);
        });
    }

    getUserByKey(key: string): Observable<User> {
        return this.db.object(`${DbService.USERS}/${key}`);
    }

    getUserFromDB(): Observable<User> {
        if (this.user == null) {
            console.warn("UserService: User is Null, maybe logged out! => returning dummy user !");
        }
        return this.getUserByKey(this.user ? this.user.$key : "-1");
    }

    getUser(): Observable<User> {
        return this._user.asObservable();
    }

    getUserSnapShot(): User {
        return this.user;
    }

    writeNotificationTokens(osPushToken: string, osUserId: string) {
        let updateData = {};
        updateData[`${DbService.USERS}/${this.user.$key}/osPushToken`] = osPushToken;
        updateData[`${DbService.USERS}/${this.user.$key}/osUserId`] = osUserId;
        return firebase.database().ref().update(updateData);
    }

    public hasMessagesUnread(userKey: string): Observable<boolean> {
        return this.db.object(`${DbService.USER_MESSAGES}/${userKey}/unreadMessages`).map((o) => {
            return o.$exists() ? o.$value : false;
        });
    }

    public updateUserMessagesUnread(userKeys: string[], reset: boolean = false) {
        let updateData = {};
        for (let userKey of userKeys) {
            console.log(`updateUserMessagesUnread: ${userKey}, ${reset}`);
            updateData[`${DbService.USER_MESSAGES}/${userKey}/unreadMessages`] = reset ? null : true;
        }
        return firebase.database().ref().update(updateData);
    }





    // hasUserPotos(key: string): Promise<boolean> {
    //     console.log("Has user photos !")
    //     return new Promise<boolean>((resolve, reject) => {
    //         this.db.object(`${UserService.USER_PICTURES}/${key}/photos`).take(1).subscribe((obj) => {
    //             console.log(`resolver ${obj.$exists()}`)
    //             resolve(obj.$exists());
    //         }, error => {
    //             reject(error);
    //         })
    //     });
    // }

    getUserPhotos(key: string): Promise<ImageInfo[]> {
        return this.dbService.getUserPhotos(key);
    }

    saveUserPhotos(photos: ImageInfo[]) {
        this.dbService.saveUserPhotos(this.user, photos);
    }

    public saveUserInfo(info: string) {
        return this.db.object(`${DbService.USERS}/${this.user.$key}/info`).set(info);
    }

    public canUpdateUserBirthdate(): Observable<boolean> {
        return this.db.object(`${DbService.USER_MODIFIED}/${this.user.$key}/birthdate`).take(1).map((time) => {
            let canUpdate = true;
            if (time.$exists()) {
                let days = moment().diff(moment(time.$value), 'days');
                canUpdate = days > 30;
            }
            return canUpdate;
        });
    }

    public updateUserBirthdate(date: string) {
        this.canUpdateUserBirthdate().subscribe(canUpdate => {
            if (canUpdate) {
                let b = moment(date, ["MM/DD/YYYY", "MM/DD", "YYYY-MM-DD"]);
                if (b.isValid()) {
                    let updateData = {};
                    updateData[`${DbService.USERS}/${this.user.$key}/birthdate`] = b.format("MM/DD/YYYY");
                    updateData[`${DbService.USERS}/${this.user.$key}/needChangeBirthday`] = null;
                    updateData[`${DbService.USER_MODIFIED}/${this.user.$key}/birthdate`] = firebase.database['ServerValue']['TIMESTAMP'] as number;

                    firebase.database().ref().update(updateData)
                }
            }

        });

    }

    public createUserIfNotExist(key: string, userData: UserData): Promise<void> {
        return new Promise<void>((resolve, reject) => {

            // check if user exists 
            this.db.object(`${DbService.USERS}/${key}`).take(1).subscribe((user: User) => {
                if ((<any>user).$exists()) {
                    // user exists => do nothing
                    console.log("createUserIfNotExist: User exists!");
                    resolve();
                }
                else {
                    // no user exists => uplaod images and create user !
                    console.log("createUserIfNotExist: create new User !");

                    let updateData = {};
                    userData.uid = null; // dont store user-uid !

                    let birthdayCheck = this.checkBirthday(userData.birthdate);
                    if (!birthdayCheck.isValid) {
                        userData.birthdate = birthdayCheck.default;
                        userData.needChangeBirthday = true;
                    }

                    userData.gender = userData.gender || 'male';
                    updateData[`${DbService.USERS}/${key}`] = userData;

                    console.log("createUserIfNotExist: try to upload profile picture");
                    this.imageService.uploadFbCloudinary(userData.image).then((imageInfo: ImageInfo) => {
                        userData.image = imageInfo.name;
                        updateData[`${DbService.USER_PICTURES}/${key}/photos`] = [imageInfo];

                        console.log("createUserIfNotExist: upload success!");
                        return imageInfo;
                    }).catch(error => {
                        console.log("createUserIfNotExist: Error uploading profile picture to cloudinary", error);
                        userData.image = userData.gender === "male" ? "m" : "f";
                        return;
                    }).then(info => {
                        console.log("createUserIfNotExist: update user in database");
                        firebase.database().ref().update(updateData).then(() => {
                            resolve();
                        }).catch(error => {
                            console.log("createUserIfNotExist: can't create user", error);
                            reject();
                        });
                    });
                }
            }, error => {
                console.log("createUserIfNotExist: error getting user", error);
                reject();
            });

        });
    }

    public blockUser(user: User) {
        let date = firebase.database['ServerValue']['TIMESTAMP'] as number;
        let updateData = {};
        updateData[`${DbService.USER_BLOCK}/${this.user.$key}/${user.$key}`] = {createDate: date };
        return firebase.database().ref().update(updateData);
    }

    public reportUser(user: User, reason: number) {
        let date = firebase.database['ServerValue']['TIMESTAMP'] as number;
        let updateData = {};
        updateData[`${DbService.REPORT_USER}/${user.$key}/${this.user.$key}`] = {createDate: date /*, userId: this.user.$key*/, reason: reason};
        return firebase.database().ref().update(updateData);
    }

    public removeUser(): firebase.Promise<any> {
        return this.dbService.removeUser(this.user).then((deleteData) => {
            console.log("UserService(removeUser) Delete Data = ", deleteData);
            // deleteData={};
            return firebase.database().ref().update(deleteData);
        });
    }


    private checkBirthday(birthdate: string): any {
        let b = moment(birthdate, ["MM/DD/YYYY", "MM/DD"]);
        let age = moment().diff(moment(birthdate, ["MM/DD/YYYY", "MM/DD"]), 'years');
        let isValid = b.isValid() && age > 0;
        return { isValid: isValid, default: "01/01/1980" };
    }
}