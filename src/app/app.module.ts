import { NgModule, ErrorHandler } from '@angular/core';

import { Http, HttpModule } from '@angular/http';

import { BrowserModule } from '@angular/platform-browser';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage, IonicStorageModule } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Facebook } from '@ionic-native/facebook';
import { Camera } from '@ionic-native/camera';
import { Transfer } from '@ionic-native/transfer';
import { Badge } from '@ionic-native/badge';
import { File } from '@ionic-native/file';
import { OneSignal } from '@ionic-native/onesignal';
import { Keyboard } from '@ionic-native/keyboard';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Globalization } from '@ionic-native/globalization';
import { AppVersion } from '@ionic-native/app-version';


import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';


// import * as firebase from 'firebase/app';

import { MomentModule } from 'angular2-moment';

import { SwingModule } from 'angular2-swing';

import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { ProfileEditPage } from '../pages/profile-edit/profile-edit';
import { ProfileHelpPage } from '../pages/profile-help/profile-help';
import { SettingsPage } from '../pages/settings/settings'; 
import { ChatsPage } from '../pages/chats/chats';
import { RequestsPage } from '../pages/requests/requests'
import { MessagesPage } from '../pages/messages/messages';
import { MessagesMenuPage } from '../pages/messages/messages-menu';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { ShoutDetailsPage } from '../pages/shout-details/shout-details';
import { TutorialPrivacyPage } from '../pages/tutorial-privacy/tutorial-privacy';
import { InfoLocationPage } from '../pages/info-location/info-location';

import { ComponentsModule } from '../components/components.module';
import { DirectrivesModule } from '../directives/directives.module';
import { PipesModule } from '../pipes/pipes.module';

import { Settings, AuthService, UserService, ShoutService, ChatService, ImageService, DbService, NotificationService } from '../providers/providers';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { environment } from '../environments/environment';


// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: Http) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    show: 'a',
    searchRange: 40,
    ageRange: { lower: 20, upper: 55 },
    notifyShout: true,
    notifyMessage: true
  });
}

/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
let pages = [
  MyApp,
  TabsPage,
  HomePage,
  ProfilePage,
  ProfileEditPage,
  ProfileHelpPage,
  SettingsPage,
  ChatsPage,
  RequestsPage,
  MessagesPage,
  MessagesMenuPage,
  TutorialPage,
  InfoLocationPage,
  ShoutDetailsPage,
  TutorialPrivacyPage,
];
 
// let pipes = [
//   // CloudinaryImgPipe,
//   // PrettyPipe
// ];

// let components = [
//   // SvgIconComponent,
//   // ChatMessageComponent, 
// ]

// let directrives = [
//   AutosizeDirectrive
// ]

export function declarations() {
  // return (<any>pages).concat(components);
  return pages;
}

export function entryComponents() {
  return pages;
}

export function providers() {
  return [
    StatusBar,
    SplashScreen,
    Geolocation,
    LocationAccuracy,
    Facebook,
    Camera,
    Transfer,
    File,
    OneSignal,
    Badge,
    Keyboard,
    SocialSharing,
    Globalization,
    AppVersion,

    AuthService,
    UserService,
    ShoutService,
    ChatService,
    ImageService,
    NotificationService,
    DbService,

    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}



@NgModule({
  declarations: declarations(),
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: true,
      spinner: 'crescent',

      scrollAssist: false, 
      autoFocusAssist: false
    }),
    IonicStorageModule.forRoot(),

    PipesModule,
    ComponentsModule,
    DirectrivesModule,

    MomentModule,
    SwingModule, 
    // SwipeCardsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    TranslateModule.forRoot({
    loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
    }
})    
  ],
  bootstrap: [IonicApp],
  entryComponents: entryComponents(),
  providers: providers()
})
export class AppModule { }
