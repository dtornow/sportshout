import { Component, ViewChild } from '@angular/core';
import { Platform, Config, NavController } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';
import { Globalization } from '@ionic-native/globalization';

import { AngularFireAuth } from 'angularfire2/auth';

import * as moment from 'moment';
import 'moment/locale/de';
//import * as locales from 'moment/min/locales';  

import { Settings } from '../providers/providers';

import { TutorialPage } from '../pages/tutorial/tutorial';
// import { MessagesPage } from '../pages/messages/messages';
import { TabsPage } from '../pages/tabs/tabs';

import { TranslateService } from '@ngx-translate/core';
import { AuthService, DbService } from '../providers/providers';

// import * as firebase from 'firebase/app';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  isAppInitialized: boolean = false;

  userId: string;

  @ViewChild('rootNav') rootNav: NavController;

  constructor(afAuth: AngularFireAuth, translate: TranslateService, platform: Platform,
    config: Config, statusBar: StatusBar, splashScreen: SplashScreen, keyboard: Keyboard, globalization: Globalization,
    settings: Settings, private authService: AuthService, private dbService: DbService) {

    platform.ready().then(() => {
      console.log("MyApp: Initialize");
      statusBar.styleDefault();
      statusBar.overlaysWebView(false);
      splashScreen.hide();

      // Set the default language for translation strings, and the current language.
      translate.setDefaultLang('de');
      globalization.getPreferredLanguage().then(language => {
        console.log(`MyApp: setting locale to ${language.value}`);
        if (language.value) {
          let locale = language.value.substring(0, 2);
          translate.use(locale);
          moment.locale(locale);
        }

        translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
          config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
        });

      });

      // Firebase Authentication
      afAuth.authState.subscribe(user => {
        if (!authService.isLoginActive) {
          if (user) {
            this.userId = user.uid;
            console.log("MyApp: Authenticated => set rootPage = TabsPage");
            this.rootNav.setRoot(TabsPage);

            //this.cleanUpDatabases();


          } else {
            this.userId = null;
            console.log("MyApp: NOT Authenticated => set rootPage = TutorialPage");
            this.rootNav.setRoot(TutorialPage);
          }
        }
      }, err => {
        this.rootPage = TutorialPage;
      });


      // // OneSignal
      // if (platform.is('cordova')) {
      //   this.oneSignal.startInit('5830c11a-a8f7-4db5-af79-527688e0fecc', '865318193572');
      //   this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert); // change this later to none !
      //   this.oneSignal.setSubscription(true);
      //   this.oneSignal.handleNotificationReceived().subscribe((v) => {
      //     console.log("MyApp: handleNotificationReceived ==> ", v)
      //     // do something when notification is received
      //   });

      //   this.oneSignal.handleNotificationOpened().subscribe((v) => {
      //     // do something when a notification is opened
      //     console.log("MyApp: handleNotificationOpened ==> ", v)
      //   });

      //   this.oneSignal.endInit();
      // }

      platform.pause.subscribe(() => {
        console.log("MyApp: onpause called");
      });

      platform.resume.subscribe(() => {
        console.log("MyApp: onresume called");

        this.cleanUpDatabases();
      });
    });
  }


  private cleanUpDatabases() {
    this.dbService.shrinkDatabase();
  }


}
