import { Component, OnInit, OnDestroy, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';

import { UserService, ShoutService } from '../../providers/providers';
import { User, ShoutRequest } from '../../models/models';

import { ShoutDetailsPage } from '../shout-details/shout-details';

import {
  StackConfig,
  // Stack,
  // Card,
  // ThrowEvent,
  // DragEvent,
  SwingStackComponent,
  SwingCardComponent,
  Direction
} from 'angular2-swing';

@Component({
  selector: 'page-requests',
  templateUrl: 'requests.html'
})
export class RequestsPage implements OnInit, OnDestroy {

  @ViewChild('myswing1') swingStack: SwingStackComponent;
  @ViewChildren('swingcards') swingCards: QueryList<SwingCardComponent>;

  shoutRequest: ShoutRequest;
  users: User[] = [];

  stackConfig: StackConfig;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private modalCtrl: ModalController,
    private userService: UserService, private shoutService: ShoutService) {
    this.shoutRequest = navParams.get("request");

    this.stackConfig = {
      allowedDirections: [Direction.LEFT, Direction.RIGHT],
      throwOutConfidence: (offsetX: number, offsetY: number, targetElement: HTMLElement) => {
        // you would put ur logic based on offset & targetelement to determine
        // what is your throwout confidence
        const xConfidence = Math.min(Math.abs(offsetX) / (targetElement.offsetWidth / 1.7), 1);
        const yConfidence = Math.min(Math.abs(offsetY) / (targetElement.offsetHeight / 2), 1);

        return Math.max(xConfidence, yConfidence);
      },
      transform: (element, x, y, r) => {
        this.onItemMove(element, x, y, r);
      },

      throwOutDistance: (d) => {
        return 800;
      }
    };
  }

  ionViewDidLoad() {
    console.log('RequestsPage: ionViewDidLoad');
  }

  ngOnInit() {
    console.log("RequestsPage: ngOnInit")

    this.shoutRequest.users.forEach((userId) => {
      this.userService.getUserByKey(userId).take(1).subscribe((user) => {
        if ((<any>user).$exists()) {
          this.users.push(user);
        }
      })
    })
  }

  ngOnDestroy() {
    console.log("RequestsPage: ngOnDestroy")
  }

  goBack() {
    this.navCtrl.pop();
  }


  onThrowIn(event) {
    let ele = event.target.getElementsByClassName("ovl");
    for (let e of ele) {
      e.style.opacity = "0";
    }
  }

  liked: boolean;

  voteUp(like: boolean) {
    console.log(`RequestsPage: voteUup(${like})`);
    this.liked = like;
  }

  onThrowOutEnd(event) {
    console.log(`RequestsPage: onThrowOutEnd(${this.liked})`);
    let removedCard = this.users.pop();
    if (this.liked) {
      this.shoutService.acceptShoutRequest(this.shoutRequest, removedCard);
    }
    else {
      this.shoutService.rejectShoutRequest(this.shoutRequest, removedCard);
    }
  }

  throwOut(like: boolean) {
    console.log(`RequestsPage: throwOut(${like})`);
    this.liked = like;
    let upperCard = this.swingCards.last.getCard();
    let ele = this.swingCards.last.getNativeElement();
    ele.className += ` throwout-${like ? '' : 'dis'}like`;
    upperCard.throwOut(like ? 1 : -1, -1);
  }


  onItemMove(element, x, y, r) {
    var abs = Math.abs(x);
    let op = abs * 6.0 / element.offsetWidth;

    let eleLike = element.getElementsByClassName(x < 0 ? "dislike" : "like");
    let eleDisLike = element.getElementsByClassName(x < 0 ? "like" : "dislike");
    if (eleLike != null) {
      eleLike[0].style.opacity = op.toString();
    }
    if (eleDisLike != null) {
      eleDisLike[0].style.opacity = "0";
    }
    element.style['transform'] = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
  }

  showUserDetails(user: User) {
    let shoutModal = this.modalCtrl.create(ShoutDetailsPage, { userId: user.$key });
    shoutModal.present();
  }
}
