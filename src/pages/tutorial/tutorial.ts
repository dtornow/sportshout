import { Component, OnInit } from '@angular/core';

import { Platform, ModalController, NavController, LoadingController } from 'ionic-angular';

import { TutorialPrivacyPage } from '../tutorial-privacy/tutorial-privacy';
import { TabsPage } from '../tabs/tabs';

import { TranslateService } from '@ngx-translate/core';

import { AuthService, Settings } from '../../providers/providers';


export interface Slide {
  description: string;
  image: string;
}

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage implements OnInit {
  slides: Slide[];
  showSkip = true;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
    public loadingController: LoadingController, public settings: Settings,
    private translate: TranslateService, private authService: AuthService) {
  }
  ngOnInit() {
    this.translate.stream([
      "TUTORIAL.SLIDE1_DESCRIPTION",
      "TUTORIAL.SLIDE2_DESCRIPTION",
      "TUTORIAL.SLIDE3_DESCRIPTION",
      "TUTORIAL.SLIDE4_DESCRIPTION"
    ]) 
      .subscribe((values) => {  

        let lang = this.translate.currentLang === 'de' ? "de" : "en";
        
        console.log("Current lang = ", this.translate.currentLang); 
        console.log("values= ", values);

        //console.log('Loaded values', values);
        this.slides = [
          {
            description: values['TUTORIAL.SLIDE1_DESCRIPTION'],
            image: `assets/img/${lang}/tutorial-1-girl.png`
          },
          {
            description: values['TUTORIAL.SLIDE2_DESCRIPTION'],
            image: `assets/img/${lang}/tutorial-2-girl.png`
          },
          {
            description: values['TUTORIAL.SLIDE3_DESCRIPTION'],
            image: `assets/img/${lang}/tutorial-3-girl.png`,
          },
          {
            description: values['TUTORIAL.SLIDE4_DESCRIPTION'],
            image: `assets/img/${lang}/tutorial-4-girl.png`
          }
        ];
      });
  }

  onSignInSuccess() {
    this.navCtrl.setRoot(TabsPage, {}, {
      animate: true,
      direction: 'forward'
    });
  }

  signInWithFacebook() {
    this.translate.get('LOADING').subscribe(v => {
      let loader = this.loadingController.create({
        content: v,
        dismissOnPageChange: true
      });

      loader.present().then(() => {
        this.authService.loginWithFacebook().then(() => {
          console.log("login Success => Setting root page");


          loader.dismiss();

          this.onSignInSuccess();
        }).catch(error => {
          console.log("login Error", error);
          loader.dismiss();
        });

      });

    });
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd;
  }

  /*
    ionViewDidEnter() {
      // the root left menu should be disabled on the tutorial page
      this.menu.enable(false);
    }
  
    ionViewWillLeave() {
      // enable the root left menu when leaving the tutorial page
      this.menu.enable(true);
    }
  */

  openTutorialPrivacy() {
    let exp = this.modalCtrl.create(TutorialPrivacyPage, { "tutorialPage": this });
    exp.present();
  }


  public openDisclaimer() {
    this.navCtrl.push('Disclaimer');
  }

  public openPrivacy() {
    this.navCtrl.push('Privacy');
  }
}
