import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { TranslateService } from '@ngx-translate/core';

import * as moment from 'moment';

import { SettingsPage } from '../settings/settings';
import { ProfileEditPage } from '../profile-edit/profile-edit';
import { ProfileHelpPage } from '../profile-help/profile-help';
import { ShoutDetailsPage } from '../shout-details/shout-details';

import { UserService, ShoutService, DbService } from '../../providers/providers';
import { User, UserShout } from '../../models/models';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage implements OnInit, OnDestroy {

  user: User;
  userSubscription: Subscription;

  shouts: UserShout[] = [];
  shoutSubscription: Subscription;

  shoutMessage: string = '';

  storingShout: boolean

  get age(): string {
    return moment().diff(moment(this.user.birthdate, '"MM/DD/YYYY"'), 'years').toString();
  }

  get canPostNewShouts(): boolean {
    return this.shouts.length < DbService.MAX_SHOUTS;
  }


  constructor(private navCtrl: NavController, private modalCtrl: ModalController, private alertCtrl: AlertController,
    private translate: TranslateService, private userService: UserService, private shoutService: ShoutService) {


  }

  /*
  @ViewChild(Content) content: Content;
 updateScroll() {
    console.log('updating scroll')
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 400)
}
*/
  ngOnInit() {
    this.userSubscription = this.userService.getUser().subscribe((u: User) => {
      this.user = u;
    });

    this.shoutSubscription = this.shoutService.getRecentUserShouts().subscribe((shouts: UserShout[]) => {
      this.shouts = shouts;
    });
  }

  ngOnDestroy() {
    console.log("ProfilePage: ngOnDestroy")
    this.shoutSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }




  openSettings() {
    this.navCtrl.push(SettingsPage);
  }


  // onInputKeypress({ keyCode }: KeyboardEvent): void {
  //   if (keyCode === 13) {
  //     this.sendShout();
  //   }
  // }


  deleteShout(key: string): void {
    this.shoutService.deleteShout(key);
  }

  sendShout(): void {
    // If message was yet to be typed, abort
    if (!this.shoutMessage) {
      return;
    }


    this.storingShout = true;
    this.shoutService.createShout(this.shoutMessage).then(() => {
      this.storingShout = false;
    }).catch(err => {
      this.storingShout = false;
      this.translate.get(['PROFILE.STORE_SHOUT_ERR_TITLE', 'PROFILE.STORE_SHOUT_ERR_SUBTITLE','BTN_OK']).subscribe(values => { 
        let alert = this.alertCtrl.create({
          title: values['PROFILE.STORE_SHOUT_ERR_TITLE'],
          subTitle: values['PROFILE.STORE_SHOUT_ERR_SUBTITLE'],
          buttons: [values['BTN_OK']]
        });
        alert.present();
      });


    });

    this.shoutMessage = '';
  }

  help() {
    this.navCtrl.push(ProfileHelpPage);
  }

  editProfile() {
    this.navCtrl.push(ProfileEditPage);
  }

  showProfileDetails() {
    let shoutModal = this.modalCtrl.create(ShoutDetailsPage, { userId: this.user.$key });
    shoutModal.present();
  }
}
