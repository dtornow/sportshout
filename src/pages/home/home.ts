import { Component, ViewChild, ViewChildren, OnInit, OnDestroy, QueryList } from '@angular/core';

import { App, NavController, ModalController, Platform } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';

import { LocationAccuracy } from '@ionic-native/location-accuracy';

import {
  StackConfig,
  // Stack,
  // Card,
  // ThrowEvent,
  // DragEvent,
  SwingStackComponent,
  SwingCardComponent,
  Direction
} from 'angular2-swing'; 

import { AuthService, ShoutService } from '../../providers/providers';

import { ShoutDetailsPage } from '../shout-details/shout-details';
import { InfoLocationPage } from '../info-location/info-location';


import { Shout } from '../../models/models';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit, OnDestroy {

  @ViewChild('myswing1') swingStack: SwingStackComponent;
  @ViewChildren('swingcards') swingCards: QueryList<SwingCardComponent>;

  shouts: Shout[] = [];

  noShoutsFound = false;

  stackConfig: StackConfig;
  shoutSubscribtion: Subscription = null;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController, public app: App,
    private authService: AuthService, private shoutService: ShoutService, private platform: Platform, private locationAccuracy: LocationAccuracy) {
    this.stackConfig = {
      allowedDirections: [Direction.LEFT, Direction.RIGHT],
      throwOutConfidence: (offsetX: number, offsetY: number, targetElement: HTMLElement) => {
        // you would put ur logic based on offset & targetelement to determine
        // what is your throwout confidence
        const xConfidence = Math.min(Math.abs(offsetX) / (targetElement.offsetWidth / 1.7), 1);
        const yConfidence = Math.min(Math.abs(offsetY) / (targetElement.offsetHeight / 2), 1);

        return Math.max(xConfidence, yConfidence);
      },
      transform: (element, x, y, r) => {
        this.onItemMove(element, x, y, r);
      },

      throwOutDistance: (d) => {
        return 800;
      }
      //maxRotation: 50
      // throwOutDistance: (d) => {
      //   //console.log("d=", d);
      //   return 400;
      // },
    };

    platform.resume.subscribe(() => {
      console.log("HomePage: onresume called");
      this.loadCheckShouts();
    });


  }

  ngOnInit() {
    console.log("HomePage: ngOnInit");
    this.loadCheckShouts();
  }

  ngOnDestroy() {
    console.log("HomePage: ngOnDestroy");
    this.unsubscribeShouts();
  }


  ionViewWillEnter() {
    console.log("HomePage: ionViewWillEnter")
    if (this.shoutService.needReloadShouts) {
      this.loadCheckShouts();
    }
  }

  ionViewWillLeave() {
    console.log("HomePage: ionViewWillLeave")
  }

  private loadShouts() {
    console.log("HomePage(loadShouts): try, to load new Shouts");

    // reset reload flag !
    this.shoutService.needReloadShouts = false;
    this.noShoutsFound = false;
    this.shouts = [];
    setTimeout(() => { this.noShoutsFound = true }, 15000);

    this.shoutSubscribtion = this.shoutService.getShoutsNearby().subscribe((val: Shout | string) => {
      if (typeof val === 'string') {
        let idx = this.shouts.findIndex((shout) => { return shout.$key === val });
        if (idx > -1) {
          this.shouts.splice(idx, 1);
        }
      }
      else {
        this.shouts.push(val);
      }
    }, err => {
      console.log("HomePage(LoadShouts): Error location etc.", err);
      if(this.platform.is('ios')) {
        this.navCtrl.push(InfoLocationPage);
      }
    });
  }


  private loadCheckShouts() {
    console.log("HomePage(loadCheckShouts): try, to load new Shouts");

    this.locationAccuracy.canRequest().then((canRequest: boolean) => {
      console.log("HomePage(loadCheckShouts): canRequest", canRequest);
      if (canRequest) {
        this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
          this.loadShouts();
          },
          error => {
            console.log('HomePage(loadCheckShouts) Error requesting location permissions', error);
          }
        );

      }
      else {
        this.loadShouts();
      }
    });
  }

  private unsubscribeShouts(): void {
    if (this.shoutSubscribtion) {
      this.shoutSubscribtion.unsubscribe();
      this.shoutSubscribtion = null;
    }
  }

  gotoProfile() {
    this.navCtrl.parent.select(0);
  }

  onThrowIn(event) {
    let ele = event.target.getElementsByClassName("ovl");
    for (let e of ele) {
      e.style.opacity = "0";
    }


  }


  onItemMove(element, x, y, r) {
    var abs = Math.abs(x);
    let op = abs * 6.0 / element.offsetWidth;

    let eleLike = element.getElementsByClassName(x < 0 ? "dislike" : "like");
    let eleDisLike = element.getElementsByClassName(x < 0 ? "like" : "dislike");
    if (eleLike != null) {
      eleLike[0].style.opacity = op.toString();
    }
    if (eleDisLike != null) {
      eleDisLike[0].style.opacity = "0";
    }
    element.style['transform'] = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
  }


  private liked: boolean;

  // Connected through HTML
  voteUp(like: boolean) {
    console.log(`HomePage: voteUup(${like})`);
    this.liked = like;
  }

  onThrowOutEnd(event) {
    console.log(`HomePage: onThrowOutEnd(${this.liked})`);
    let removedCard = this.shouts.pop();
    //    console.log("HomePage: onThrowOutEnd()" + this.liked, removedCard, event);
    this.shoutService.voteShout(removedCard, this.liked);
  }

  throwOut(like: boolean) {
    console.log(`HomePage: throwOut(${like})`);
    this.liked = like;
    let upperCard = this.swingCards.last.getCard();
    let ele = this.swingCards.last.getNativeElement();
    ele.className += ` throwout-${like ? '' : 'dis'}like`;
    upperCard.throwOut(like ? 1 : -1, -1);
    //  ele.addEventListener("transitionend", ()=>{
    //  }, true);


    //upperCard.throwOut(like ? 1 : -1, -1);


  }


  // Add new cards to our array
  // addNewCards(count: number) {
  //   this.http.get('https://randomuser.me/api/?results=' + count)
  //     .map(data => data.json().results)
  //     .subscribe(result => {
  //       for (let val of result) {
  //         this.cards.push(val);
  //       }
  //     })
  // }

  trackByShoutId(index: number, shout: Shout) {
    return shout.$key;
  }

  showShoutDetails(shout: Shout) {
    console.log("clickwd ", shout)
    let shoutModal = this.modalCtrl.create(ShoutDetailsPage, { shout: shout });
    shoutModal.onDidDismiss(data => {
      if (data.vote) {
        this.throwOut(data.liked);
      }


    });
    shoutModal.present();

    //this.navCtrl.push(ShoutDetailsPage, {shout:shout, callback: this.afterShoutDetailsReturns});
  }

  doRefresh(refresher) {
    this.loadShouts();
    setTimeout(() => {
      refresher.complete();
    }, 1000);
  }
}
