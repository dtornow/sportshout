import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { NavController, PopoverController, ModalController, AlertController, NavParams, Content } from 'ionic-angular';
import { Subscription } from 'rxjs/Subscription';
import { TranslateService } from '@ngx-translate/core';

import { ChatService, ShoutService, UserService, AuthService } from '../../providers/providers';
import { Chat, ChatMessage } from '../../models/models';

import { MessagesMenuPage } from './messages-menu';
import { ShoutDetailsPage } from '../shout-details/shout-details';

@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html'
})
export class MessagesPage implements OnInit, OnDestroy {

  chat: Chat;
  messages: ChatMessage[];


  message: string = '';
  chatId: string;

  loadingMessages: boolean = true;

  chatSubscription: Subscription;

  @ViewChild(Content) content: Content;

  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
    public popoverCtrl: PopoverController, private alertCtrl: AlertController,
    private translate: TranslateService,
    public navParams: NavParams, public chatService: ChatService, public shoutService: ShoutService, private authService: AuthService, private userService: UserService) {
    this.chatId = navParams.get('chatId');

    this.loadingMessages = true;
  }

  ionViewDidEnter() {
    this.content.scrollToBottom(300);//300ms animation speed

  }

  get ownUserId() {
    return this.authService.currentUser.uid;
  }

  get isOwner() {
    return this.authService.currentUser.uid === this.chat.ownerId;
  }

  // public getMessage(msg: ChatMessage): string {
  //   return msg.type == ChatService.CHAT_MESSAGE_USER_LEAVED ? `hat den Chat verlassen`: msg.message;
  // }

  // public isUserLeaved(msg: ChatMessage): boolean {
  //   return msg.type == ChatService.CHAT_MESSAGE_USER_LEAVED;
  // }

  // public isMessageText(msg: ChatMessage): boolean {
  //   return msg.type == ChatService.CHAT_MESSAGE_TEXT;
  // }

  ngOnInit() {
    console.log("MessagesPage: ngOnInit")

    //this.messages.subscribeOn
    this.chatService.getChat(this.chatId).take(1).subscribe((chat) => {
      this.chat = chat;
    });

    this.chatSubscription = this.chatService.getChatMessages(this.chatId).subscribe((chatMessages) => {
      this.messages = chatMessages;

      this.loadingMessages = false;

      setTimeout(() => {
        this.content.scrollToBottom();//300ms animation speed
      });

    });
  }

  ngOnDestroy() {
    console.log("MessagesPage: ngOnDestroy");

    if (this.chatSubscription) {
      this.chatSubscription.unsubscribe();
    }
    // this.autoScroller.disconnect();
  }

  // onInputKeypress({ keyCode }: KeyboardEvent): void {
  //   if (keyCode === 13) {
  //     this.sendMessage();
  //   }
  // }

  sendMessage(): void {
    // If message was yet to be typed, abort

    if (!this.message) {
      return;
    }

    this.chatService.createChatMessage(this.chatId, this.message);
    this.message = '';
  }

  showOptions(myEvent) {
    //let isOwner = this.authService.currentUser.uid === this.chat.ownerId;
    let popover = this.popoverCtrl.create(MessagesMenuPage, { owner: this.isOwner }, { cssClass: 'messages-menu' });
    popover.onDidDismiss((data) => {
      if (data) {
        switch (data.command) {
          case 'delete':
            this.deleteOrLeave();
            break;
          case 'manage':
            this.manageChat();
            break;
          default:
            break;
        }
      }
    })
    popover.present({ ev: myEvent });
  }

  private deleteOrLeave() {
    this.translate.get(['MESSAGES.LEAVE_TITLE', 'MESSAGES.LEAVE_TEXT', 'MESSAGES.DEL_TITLE', 'MESSAGES.DEL_TEXT', 'BTN_CANCEL', 'MESSAGES.BTN_LEAVE', 'MESSAGES.BTN_DEL']).subscribe(values => {

      let confirm = this.alertCtrl.create({
        title: values[this.isOwner ? 'MESSAGES.DEL_TITLE' : 'MESSAGES.LEAVE_TITLE'],
        message: values[this.isOwner ? 'MESSAGES.DEL_TEXT' : 'MESSAGES.LEAVE_TEXT'],
        buttons: [
          {
            text: values['BTN_CANCEL'],
            handler: () => {
              console.log('Do nothing !');
            }
          },
          {
            text: values[this.isOwner ? 'MESSAGES.BTN_DEL' : 'MESSAGES.BTN_LEAVE'],
            cssClass: 'delButton',
            handler: () => {
              this.chatService.leaveOrDeleteChat(this.chatId);

              // go back to chats overview !
              this.navCtrl.pop();
            }
          }
        ]
      });
      confirm.present();
    });

  }

  showProfile(userId: string) {
    this.userService.getUserByKey(userId).take(1).subscribe(u => {
      if ((<any>u).$exists()) {
        let shoutModal = this.modalCtrl.create(ShoutDetailsPage, { userId: userId });
        shoutModal.present();
      }
    });

  }

  manageChat() {
    if (this.isOwner) {
      this.navCtrl.push('ChatManagePage', { chatId: this.chat.$key });
    }
  }

}



