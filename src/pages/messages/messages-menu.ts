import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';

// import { TranslateService } from '@ngx-translate/core';


@Component({
    template: `
      <ion-list>  
          <ion-item *ngIf="owner" (click)="close('manage')">{{'MESSAGES.BTN_MANAGE' | translate}}</ion-item>
          <ion-item (click)="close('delete')">{{(owner ? 'MESSAGES.BTN_DEL' : 'MESSAGES.BTN_LEAVE') | translate}}</ion-item>
      </ion-list>
  `
})
export class MessagesMenuPage {
    // deleteText;
    owner

    constructor(public viewCtrl: ViewController, navParams: NavParams) {
        this.owner = navParams.get("owner");

        // translate.get([
        //     "MESSAGES.BTN_LEAVE",
        //     "MESSAGES.BTN_DEL"
        // ]).subscribe((values) => {
        //     this.deleteText = this.owner ? values['MESSAGES.BTN_DEL'] : values['MESSAGES.BTN_LEAVE']
        // });
    }


    close(command) {
        this.viewCtrl.dismiss({ command: command });
    }
}
