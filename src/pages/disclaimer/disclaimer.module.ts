import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { Disclaimer } from './disclaimer';

@NgModule({
  declarations: [
    Disclaimer,
  ],
  imports: [
    IonicPageModule.forChild(Disclaimer),
    TranslateModule.forChild()
  ],
  exports: [
    Disclaimer
  ]
})
export class DisclaimerModule {}
