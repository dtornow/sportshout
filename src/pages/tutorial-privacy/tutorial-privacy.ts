import { Component } from '@angular/core';

import { ViewController, NavParams } from 'ionic-angular';

import { TutorialPage } from '../tutorial/tutorial';


@Component({
  selector: 'page-tutorial-privacy',
  templateUrl: 'tutorial-privacy.html'
})
export class TutorialPrivacyPage {

  tutorialPage: TutorialPage;

  constructor(public viewCtrl: ViewController, params: NavParams) {
    this.tutorialPage = params.get("tutorialPage");
  }

  close() {
    this.viewCtrl.dismiss();
  }

  signInWithFacebook() {
     this.viewCtrl.dismiss().then(()=>{
      this.tutorialPage.signInWithFacebook();
     });
  }



}
