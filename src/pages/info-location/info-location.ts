import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-info-location',
  templateUrl: 'info-location.html',
})
export class InfoLocationPage {

  constructor(public navCtrl: NavController) {
  }

  back() {
    this.navCtrl.pop();
  }

}
