import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';


import { UserService, ImageService } from "../../providers/providers";
import { User, ImageInfo } from "../../models/models";

import * as moment from 'moment';

// declare var cordova: any;

@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html'
})
export class ProfileEditPage {
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public formBuilder: FormBuilder,
    private imageService: ImageService, private userService: UserService) {

  }

  showFavourite: boolean = false;
  showChange: boolean = false;
  selectedPhoto: number[] = [];

  user: User;
  userPictures: ImageInfo[] = [];

  form: FormGroup; 
  birthdayDisabled: boolean = false;
  formReady = false;

  _buildForm() {
    let birth = moment(this.user.birthdate, ["MM/DD/YYYY", "MM/DD"]).format();
    let group: any = {
      info: [this.user.info],
      birthdate: [{value: birth, disabled:this.birthdayDisabled}],
    };
    this.form = this.formBuilder.group(group);

    //this.disableBirthday = this.user.needChangeBirthday;

    // Watch the form for changes, and
    // this.form.valueChanges.subscribe((v) => {
    //  console.log("Value changed ! ", v, this.form.value)
    // });
  }

  isSelected(index: number) {
    return this.selectedPhoto.findIndex(i => { return i == index; }) > -1;
  }

 ionViewDidLoad() { 
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});
  }

  ionViewWillEnter() {
    this.user = this.userService.getUserSnapShot();
    this._buildForm();
    

//    this.userInfo = this.user.info;
    this.userService.getUserPhotos(this.user.$key).then((pictures: ImageInfo[]) => {
      this.userPictures = pictures;
      this.formReady = true;
    });


   
  }

  ionViewWillLeave() {
    this.userService.saveUserPhotos(this.userPictures);

    if (this.user.info != this.form.value.info) {
      this.userService.saveUserInfo(this.form.value.info);
    }

    if(moment(this.form.value.birthdate).format("MM/DD/YYYY") != this.user.birthdate) {
      this.userService.updateUserBirthdate(this.form.value.birthdate);
    }
  }

  uploadPhoto(index: number) {
    this.imageService.choosePicture().then((image: string) => {
      console.log("Choose got image", image)
      let loading = this.loadingCtrl.create();
      loading.present();

      this.imageService.uploadImageCloudinary(this.user.$key, index, image).then((imgInfo: ImageInfo) => {
        this.userPictures[index] = imgInfo;
        loading.dismiss();
      }).catch(err => {
        loading.dismiss();
      });
    }).catch(err => {
      console.log("Upload canceled!");
    });
  }

  deletePhoto(index: number) {
    console.log("delete photo index=" + index);

    this.imageService.deletePicture(this.userPictures[index].name);
    this.userPictures[index] = null;

    this.selectedPhoto = [];
    this.checkNavButtonState();
  }

  selectPhoto(index: number) {
    if (this.selectedPhoto.length < 2) {
      let idx = this.selectedPhoto.findIndex(i => { return i == index; });
      if (idx > -1) {
        this.selectedPhoto = [];
      }
      else {
        this.selectedPhoto.push(index);
      }
    }
    else {
      let idx = this.selectedPhoto.findIndex(i => { return i == index; });
      if (idx > -1) {
        this.selectedPhoto.splice(idx, 1);
      }
    }
    this.checkNavButtonState();
  }

  switchPhotos() {
    if (this.selectedPhoto.length == 2) {
      let s = this.userPictures[this.selectedPhoto[0]];
      this.userPictures[this.selectedPhoto[0]] = this.userPictures[this.selectedPhoto[1]];
      this.userPictures[this.selectedPhoto[1]] = s;

      this.selectedPhoto = [];
      this.checkNavButtonState();
    }
  }

  makeFavourite() {
    if (this.selectedPhoto.length == 1) {
      let s = this.userPictures[0];
      this.userPictures[0] = this.userPictures[this.selectedPhoto[0]];
      this.userPictures[this.selectedPhoto[0]] = s;

      this.selectedPhoto = [];
      this.checkNavButtonState();
    }
  }

  private checkNavButtonState() {
    this.showFavourite = this.selectedPhoto.length == 1 && this.selectedPhoto[0] != 0;
    this.showChange = this.selectedPhoto.length == 2;
    console.log(`NavbuttonState = showFavourite=${this.showFavourite}, showChange=${this.showChange})`);
  }



}
