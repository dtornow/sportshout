import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NavController, AlertController, NavParams, List } from 'ionic-angular';
import { Subject } from 'rxjs/Subject';
import { TranslateService } from '@ngx-translate/core';

import 'rxjs/add/operator/takeUntil';

import { UserService, AuthService, ChatService, ShoutService, NotificationService } from '../../providers/providers';
import { User, UserChat, Chat, ShoutRequest } from '../../models/models';


import { RequestsPage } from '../requests/requests';
import { MessagesPage } from '../messages/messages';

@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html'
})
export class ChatsPage implements OnInit, OnDestroy {

  private destroyed$: Subject<boolean> = new Subject();

  requests: ShoutRequest[];
  chats: UserChat[];
  user: User;

  @ViewChild('requestlist', { read: List }) requestlist: List;
  @ViewChild('chatlist', { read: List }) chatlist: List;

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, private translate: TranslateService,
    private chatService: ChatService, private shoutService: ShoutService, private userService: UserService, private authService: AuthService,
    private notificationService: NotificationService) {

  }

  get hasContent(): boolean {
    return (this.requests != null && this.requests.length > 0) || (this.chats != null && this.chats.length > 0);
  }

  ngOnInit() {
    console.log("ChatsPage: ngOnInit");
    this.shoutService.getShoutRequestsAndUsers().takeUntil(this.destroyed$).subscribe((r: ShoutRequest[]) => {
      this.requests = r;
    });


    this.chatService.getUserChats().takeUntil(this.destroyed$).subscribe((chats) => {
      this.chats = chats;
    });

    this.userService.getUser().takeUntil(this.destroyed$).subscribe((u: User) => {
      this.user = u;
    });

  }

  ngOnDestroy() {
    console.log("ChatsPage: ngOnDestroy");
    this.destroyed$.next(true);
  }

  ionViewDidEnter() {
    let unread = this.calcUnreadMessages();
    console.log(`ChatsPage: ionViewDidEnter unrweadMessages = ${unread}`);
    if (unread == 0) {
      console.log(`ChatsPage: IF ${this.user.$key}`);
      this.userService.updateUserMessagesUnread([this.user.$key], true);
      this.notificationService.clearAppBadge();
    }
  }


  private calcUnreadMessages(): number {
    let unread = null;
    if (this.chats && this.requests) {
      unread = 0;
      for (let ch of this.chats) {
        unread += ch.unreadMessages;
      }
      for (let r of this.requests) {
        if (r.isNew) {
          unread++;
        }
      }

    }
    return unread;
  }



  openRequest(request: ShoutRequest) {
    this.shoutService.markShoutRequest(request);
    this.navCtrl.push(RequestsPage, { request: request });
  }

  openChat(userChat: UserChat) {
    console.log(`Open Chat with Key ${userChat.$key}`);
    this.chatService.markUserChatAsRead(userChat.$key);

    this.navCtrl.push(MessagesPage, { 'chatId': userChat.$key });

  }

  deleteChat(userChat: UserChat) {
    userChat.chat.take(1).subscribe((c: Chat) => {
      console.log(`Delete Chat with Key ${c.$key}`);
      let isOwner = this.authService.currentUser.uid === c.ownerId;
      this.translate.get(['MESSAGES.LEAVE_TITLE', 'MESSAGES.LEAVE_TEXT', 'MESSAGES.DEL_TITLE', 'MESSAGES.DEL_TEXT', 'BTN_CANCEL', 'MESSAGES.BTN_LEAVE', 'MESSAGES.BTN_DEL']).subscribe(values => {

        let confirm = this.alertCtrl.create({
          title: values[isOwner ? 'MESSAGES.DEL_TITLE' : 'MESSAGES.LEAVE_TITLE'],
          message: values[isOwner ? 'MESSAGES.DEL_TEXT' : 'MESSAGES.LEAVE_TEXT'],
          buttons: [
            {
              text: values['BTN_CANCEL'],
              handler: () => {
                console.log('Do nothing !');
                this.chatlist.closeSlidingItems();
              }
            },
            {
              text: values[isOwner ? 'MESSAGES.BTN_DEL' : 'MESSAGES.BTN_LEAVE'],
              cssClass: 'delButton',
              handler: () => {
                this.chatlist.closeSlidingItems();
                this.chatService.leaveOrDeleteChat(c.$key);
              }
            }
          ]
        });
        confirm.present();
      });
    });
  }

  deleteRequest(request: ShoutRequest) {
    console.log(`deleteRequest Key ${request.$key}`);
      this.translate.get(['CHATS.DEL_REQUEST_TITLE', 'CHATS.DEL_REQUEST_TEXT', 'BTN_CANCEL', 'BTN_DELETE']).subscribe(values => {

        let confirm = this.alertCtrl.create({
          title: values['CHATS.DEL_REQUEST_TITLE'],
          message: values['CHATS.DEL_REQUEST_TEXT'],
          buttons: [
            {
              text: values['BTN_CANCEL'],
              handler: () => {
                this.requestlist.closeSlidingItems();
              }
            },
            {
              text: values['BTN_DELETE'],
              cssClass: 'delButton',
              handler: () => {
                this.requestlist.closeSlidingItems();
                this.shoutService.deleteShoutRequest(request);
              }
            }
          ]
        });
        confirm.present();
      });
  }

  createShout() {
    this.navCtrl.parent.select(0);

  }




}
