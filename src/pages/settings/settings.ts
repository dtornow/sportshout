import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NavController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import { SocialSharing } from '@ionic-native/social-sharing';
import { AppVersion } from '@ionic-native/app-version';

import { AuthService, UserService, NotificationService, ShoutService, Settings } from '../../providers/providers';
import { AppSettings, User } from '../../models/models'

import { TutorialPage } from '../tutorial/tutorial';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {

  settingsReady = false;
  options: AppSettings;

  versionNumber: string;
  appName:string;
  form: FormGroup;



  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public loadingCtrl: LoadingController, public navParams: NavParams, private appVersion: AppVersion,
    public formBuilder: FormBuilder, private socialSharing: SocialSharing, private translate: TranslateService,
    private authService: AuthService, private userService: UserService, private notificationService: NotificationService, private shoutService: ShoutService, private settings: Settings) {

      this.appVersion.getVersionNumber().then(v=>{
        this.versionNumber = v;
      });
      this.appVersion.getAppName().then(v=>{
        this.appName = v;
      });

  }

  _buildForm() {
    console.log("SettingsPage: _buildForm", this.options, this.settingsReady);
    let group: any = {
      show: [this.options.show],
      searchRange: [this.options.searchRange],
      ageRange: [this.options.ageRange],
      notifyMessage: [this.options.notifyMessage],
      notifyShout: [this.options.notifyShout],
    };


    this.form = this.formBuilder.group(group);

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.settings.merge(this.form.value);
    });
  }

  notificationChanged(value) {
    this.notificationService.setSubscription(value);
  }

  ionViewDidLoad() {
    // Build an empty form for the template to render
    this.form = this.formBuilder.group({});
  }

  ionViewWillEnter() {
    // Build an empty form for the template to render
    this.settingsReady = false;
    this.form = this.formBuilder.group({});

    console.log("SettingsPage: Try loading settings");
    this.settings.load().then(() => {
      this.settingsReady = true;
      this.options = this.settings.allSettings;

      this._buildForm();
    }).catch(err => {
      console.error("SettingsPage: Error loading Settings", err)
    });
  }

  ionViewWillLeave() {
    if (this.form.dirty) {
      this.shoutService.needReloadShouts = true;
    }

    // check if the Application Id has changed    
    this.userService.getUserFromDB().take(1).subscribe((user: User) => {
      if ((<any>user).$exists()) {
        this.notificationService.getApplicationIds().then(v => {
          if (user.osPushToken != v.pushToken || user.osUserId != v.userId) {
            console.log("SettingsPage: need to update the Notification Id's :", v, user.$key);
            this.userService.writeNotificationTokens(v.pushToken, v.userId).then(() => {
              console.log(`SettingsPage: Notification Id's updated`);
            });
          }
        });
      }
    })
  }


  // ngOnChanges() {
  //   console.log('Ng All Changes');
  // }



  logout() {
    this.gotoTuturialPage();
  }

  share() {
    this.translate.get(['SETTINGS.SHARE_MESSAGE', 'SETTINGS.SHARE_SUBJECT', 'SETTINGS.SHARE_TITLE']).subscribe(values => {

      let options = {
        message: values['SETTINGS.SHARE_MESSAGE'], // not supported on some apps (Facebook, Instagram)
        subject: values['SETTINGS.SHARE_SUBJECT'], // fi. for email
        url: 'http://www.sportshout.de',
        chooserTitle: values['SETTINGS.SHARE_TITLE'] // Android only, you can override the default share sheet title
      }

      this.socialSharing.shareWithOptions(options);
    });


  }






  deleteAccount() {
    this.translate.get(['SETTINGS.DELETE_TITLE', 'SETTINGS.DELETE_MESSAGE', 'BTN_CANCEL', 'BTN_DELETE']).subscribe(values => {

      let confirm = this.alertCtrl.create({
        title: values['SETTINGS.DELETE_TITLE'],
        message: values['SETTINGS.DELETE_MESSAGE'],
        buttons: [
          {
            text: values['BTN_CANCEL'],
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: values['BTN_DELETE'],
            cssClass: 'delButton',
            handler: () => {
              let loading = this.loadingCtrl.create();
              loading.present();
              this.userService.removeUser().then(() => {
                console.log("After remove user !")
                return loading.dismiss();
              }).then(() => {
                this.gotoTuturialPage();
              });
            }
          }
        ]
      });
      confirm.present();

    });
  }


  private gotoTuturialPage() {
    return this.navCtrl.setRoot(TutorialPage, {}, {
      animate: false,
      direction: 'forward'
    }).then(() => {
      this.authService.signOut();
    })
  }

  public openDisclaimer() {
    this.navCtrl.push('Disclaimer');
  }


  public openPrivacy() {
    this.navCtrl.push('Privacy');
  }


}
