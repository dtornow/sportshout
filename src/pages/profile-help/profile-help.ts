import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { DbService } from '../../providers/providers';


@Component({
  selector: 'page-profile-help',
  templateUrl: 'profile-help.html',
})
export class ProfileHelpPage {

  maxShoutCount = 2;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.maxShoutCount = DbService.MAX_SHOUTS;
  }

}
