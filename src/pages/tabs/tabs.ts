import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { NavController, AlertController, Tabs } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/takeUntil';

import { AngularFireAuth } from 'angularfire2/auth';

import { HomePage } from '../home/home';
import { ProfilePage } from '../profile/profile';
import { ChatsPage } from '../chats/chats';

import { UserService, NotificationService } from '../../providers/providers';
import { ProfileEditPage } from '../../pages/profile-edit/profile-edit';

import { User } from '../../models/models';

import * as firebase from 'firebase/app';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage implements OnInit, OnDestroy {

  @ViewChild("rootTabs") rootTabs: Tabs;

  tab1Root: any = ProfilePage;
  tab2Root: any = HomePage;
  tab3Root: any = ChatsPage;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";

  private destroyed$: Subject<boolean> = new Subject();


  hasMessagesUnread = false;
  user: User

  constructor(afAuth: AngularFireAuth, public navCtrl: NavController, private alertCtrl: AlertController,
    public translateService: TranslateService, private userService: UserService, private notificationService: NotificationService) {
    translateService.get(['TABS.TAB1_TITLE', 'TABS.TAB2_TITLE', 'TABS.TAB3_TITLE']).subscribe(values => {
      this.tab1Title = values['TABS.TAB1_TITLE'];
      this.tab2Title = values['TABS.TAB2_TITLE'];
      this.tab3Title = values['TABS.TAB3_TITLE'];
    });


    afAuth.authState.subscribe((fbUser: firebase.User) => {
      if (!fbUser) {
        this.destroyed$.next(true);
      }
    });


    // subscribe to opened notifications
    this.notificationService.notificationReceived.subscribe(data => {
      if (data) {
        // test !!
        this.rootTabs.select(2);



        // switch (data['type']) {
        //   case 'message':
        //     let chatId = data['chatId'];
        //     console.log(`Goto Messages Page with chatId = ${chatId}`)
        //     break;

        //   default:
        //     break;
        // }



      }
    });



  }


  ngOnInit() {
    console.log("TabsPage: ngOnInit");

    this.userService.getUser().takeUntil(this.destroyed$).subscribe((user: User) => {
      console.log("TabsPage: got User = ", user)
      if (user) {
        this.user = user;
        this.userService.hasMessagesUnread(user.$key).takeUntil(this.destroyed$).subscribe((unread: boolean) => {
          console.log("TabsPage: MessagesUnread = ", unread);
          this.hasMessagesUnread = unread;
        })


        if (user.needChangeBirthday) {
          this.translateService.get(['TABS.BIRTHDAY_TITLE', 'TABS.BIRTHDAY_SUBTITLE', 'TABS.BIRTHDAY_MESSAGE','BTN_OK']).subscribe(values => {
            let alert = this.alertCtrl.create({
              title: values['TABS.BIRTHDAY_TITLE'],
              subTitle: values['TABS.BIRTHDAY_SUBTITLE'],
              message: values['TABS.BIRTHDAY_MESSAGE'],
              buttons: [{
                text: values['BTN_OK'],
                handler: () => {
                  // user has clicked the alert button
                  // begin the alert's dismiss transition
                  alert.dismiss().then(() => {
                    this.navCtrl.push(ProfileEditPage);
                  });
                  return false;
                }
              }]
            });
            alert.present();
          });
        }
      }
    });
  }

  ngOnDestroy() {
    console.log("TabsPage: ngOnDestroy");
    // this.destroyed$.next(true);
  }
}
