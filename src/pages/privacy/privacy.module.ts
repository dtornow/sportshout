import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { Privacy } from './privacy';

@NgModule({
  declarations: [
    Privacy,
  ],
  imports: [
    IonicPageModule.forChild(Privacy),
    TranslateModule.forChild()
  ],
  exports: [
    Privacy
  ]
})
export class PrivacyModule {}
