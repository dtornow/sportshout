import { Component } from '@angular/core';
import { ViewController, NavParams, ActionSheetController, AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

import * as moment from 'moment';

import { ShoutService, UserService } from '../../providers/providers';

import { User, Shout, ImageInfo } from '../../models/models';

@Component({
  selector: 'page-shout-details',
  templateUrl: 'shout-details.html',
})
export class ShoutDetailsPage {

  user: User;
  shout: Shout;
  images: ImageInfo[];

  nearDistanceText: string

  finishedLoading = false;

  constructor(public viewCtrl: ViewController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, public alertCtrl: AlertController,
    private translate: TranslateService, public shoutService: ShoutService, public userService: UserService) {
    this.shout = navParams.get("shout");
    let usrId: string = navParams.get("userId");
    let userId = this.shout ? this.shout.userId : usrId;

    this.translate.get(['SHOUT_DETAILS.DISTANCE_NEAR']).subscribe(values => {
      this.nearDistanceText = values['SHOUT_DETAILS.DISTANCE_NEAR'];
    });

    // get user
    this.userService.getUserByKey(userId).take(1).subscribe((user: User) => {
      this.user = user;
      //console.log("User loaded =", this.user);

      this.userService.getUserPhotos(userId).then((images: ImageInfo[]) => {
        this.images = images;
        //console.log("Images loaded =", this.images);
      }).then(() => {
        if (this.images.length == 0) {
          this.images = [{ name: this.user.gender === "male" ? "m" : "f", format: 'png' }]
        }
        this.finishedLoading = true;
        //console.log("ShoutDetailsPage: this.images = ", this.images, this.user);
      });
    });
  }
  get age(): string {
    return moment().diff(moment(this.user.birthdate, '"MM/DD/YYYY"'), 'years').toString();
  }

  ionViewDidLoad() {
    console.log('ShoutDetailsPage: ionViewDidLoad ShoutDetails');
  }

  goBack() {
    this.viewCtrl.dismiss({ vote: false });
  }

  vote(like: boolean) {
    this.viewCtrl.dismiss({ vote: true, liked: like });


  }


  units = { unit: 'km', smallUnit: 'm', factor: 1000, smallBorder: 0.9 }

  get distance(): string {
    if (this.shout) {
      //let near = this.translate.instant('SHOUT_DETAILS.DISTANCE_NEAR');

      const formatter = new Intl.NumberFormat('de', { maximumSignificantDigits: 2 });
      let dist = this.shout.distance;

      if (dist < this.units.smallBorder) {
        let distance = dist * this.units.factor
        if (distance < 40) return `${this.nearDistanceText} ${this.units.smallUnit}`

        distance = Math.round(distance / 50) * 50
        return `${formatter.format(distance)} ${this.units.smallUnit}`
      }

      return `${formatter.format(dist)} ${this.units.unit}`

    }
    else return null;
  }


  showOptions(myEvent) {
    this.translate.get(['SHOUT_DETAILS.ACTION_REPORT', 'SHOUT_DETAILS.ACTION_BLOCK', 'BTN_CANCEL'], { name: this.user.name }).subscribe(values => {

      let actionSheet = this.actionSheetCtrl.create({
        //title: this.user.name,
        buttons: [
          {
            text: values['SHOUT_DETAILS.ACTION_REPORT'],
            role: 'destructive',
            handler: () => {
              this.reportUser();
            }
          },
          {
            text: values['SHOUT_DETAILS.ACTION_BLOCK'],
            role: 'destructive',
            handler: () => {
              this.blockUser();
            }
          },
          {
            text: values['BTN_CANCEL'],
            role: 'cancel',
            handler: () => {
              // currently do nothing !
            }
          }
        ]
      });

      actionSheet.present();
    });
  }

  private reportUser() {
    this.translate.get(['SHOUT_DETAILS.REPORTUSER_TITLE', 'SHOUT_DETAILS.REPORTUSER_MESSAGE', 'SHOUT_DETAILS.REPORTUSER_BTN_PHOTOS', 
    'SHOUT_DETAILS.REPORTUSER_BTN_SPAM', 'SHOUT_DETAILS.REPORTUSER_BTN_OTHER', 'BTN_CANCEL'], { name: this.user.name }).subscribe(values => {

      let confirm = this.alertCtrl.create({
        title: values['SHOUT_DETAILS.REPORTUSER_TITLE'],
        message: values['SHOUT_DETAILS.REPORTUSER_MESSAGE'],
        buttons: [
          {
            text: values['SHOUT_DETAILS.REPORTUSER_BTN_PHOTOS'],
            handler: () => {
              this.userService.reportUser(this.user, 1);
            }
          },
          {
            text: values['SHOUT_DETAILS.REPORTUSER_BTN_SPAM'],
            handler: () => {
              this.userService.reportUser(this.user, 2);
            }
          },
          {
            text: values['SHOUT_DETAILS.REPORTUSER_BTN_OTHER'],
            handler: () => {
              this.userService.reportUser(this.user, 3);
            }
          },
          {
            text: values['BTN_CANCEL'],
            role: 'cancel',
            handler: () => {
              // do nothing
            }
          }
        ]
      });
      confirm.present();

    });
  }

  private blockUser() {
    this.translate.get(['SHOUT_DETAILS.BLOCKUSER_TITLE', 'SHOUT_DETAILS.BLOCKUSER_MESSAGE', 'BTN_CANCEL', 'SHOUT_DETAILS.BTN_BLOCK'], { name: this.user.name }).subscribe(values => {

      let confirm = this.alertCtrl.create({
        title: values['SHOUT_DETAILS.BLOCKUSER_TITLE'],
        message: values['SHOUT_DETAILS.BLOCKUSER_MESSAGE'],
        buttons: [
          {
            text: values['BTN_CANCEL'],
            handler: () => {
              // do nothing
            }
          },
          {
            text: values['SHOUT_DETAILS.BTN_BLOCK'],
            cssClass: 'delButton',
            handler: () => {
              this.userService.blockUser(this.user);
            }
          }
        ]
      });
      confirm.present();

    });
  }


}
