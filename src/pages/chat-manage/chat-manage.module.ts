import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'angular2-moment';

import { PipesModule } from '../../pipes/pipes.module';
import { ComponentsModule } from '../../components/components.module';

import { ChatManagePage } from './chat-manage';

@NgModule({
  schemas: [],
  declarations: [
    ChatManagePage,
  ],
  imports: [
    IonicPageModule.forChild(ChatManagePage),
    TranslateModule.forChild(),
    MomentModule,
    PipesModule,
    ComponentsModule

  ],
  exports: [
    ChatManagePage
  ]
})
export class ChatManagePageModule {}
