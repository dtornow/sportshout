import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { IonicPage, NavController, AlertController, ModalController, NavParams, List } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/observable/forkJoin';

import { ShoutDetailsPage } from '../shout-details/shout-details';

import { TranslateService } from '@ngx-translate/core';


import { ChatService, UserService } from '../../providers/providers';
import { Chat, User } from '../../models/models';

@IonicPage()
@Component({
  selector: 'page-chat-manage',
  templateUrl: 'chat-manage.html',
})
export class ChatManagePage implements OnInit, OnDestroy {

  private loading: boolean = false;
  private chatId: string;
  private members: User[] = [];
  private chat: Chat;
  private destroyed$: Subject<boolean> = new Subject();

  @ViewChild('mbrs', { read: List }) list: List;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public alertCtrl: AlertController, private translate: TranslateService, 
    private chatService: ChatService, private userService: UserService) {
    this.chatId = navParams.get('chatId');
  }

  ngOnInit() {
    console.log("ChatManagePage: ngOnInit")

    this.loading = true;
    this.chatService.getChat(this.chatId).take(1).subscribe(chat => {
      this.chat = chat;

      this.chatService.getChatUsers(this.chatId).takeUntil(this.destroyed$).subscribe((userIds) => {
        console.log("ChatManagePage: Got chat users ", userIds);
        // get all the user objects
        let usr = [];
        this.members = [];
        for (let userId of userIds) {
          usr.push(this.userService.getUserByKey(userId.$key).take(1));
        }
        Observable.forkJoin(usr).subscribe((users: User[]) => {
          this.members = users;
        }, err => { }, () => { this.loading = false; });
      });
    })

  }


  ngOnDestroy() {
    console.log("ChatManagePage: ngOnDestroy");
    this.destroyed$.next(true);
  }

  deleteMember(userId: string, username: string) {
    this.translate.get(['CHAT_MANAGE.DELETE_TITLE', 'CHAT_MANAGE.DELETE_MESSAGE', 'BTN_CANCEL', 'BTN_DELETE'], { name: username }).subscribe(values => {

      let confirm = this.alertCtrl.create({
        title: values['CHAT_MANAGE.DELETE_TITLE'],
        message: values['CHAT_MANAGE.DELETE_MESSAGE'],
        buttons: [
          {
            text: values['BTN_CANCEL'],
            handler: () => {
              this.list.closeSlidingItems();
            }
          },
          {
            text: values['BTN_DELETE'],
            cssClass: 'delButton',
            handler: () => {
              this.list.closeSlidingItems();
              this.chatService.removeUserFromChat(this.chatId, userId);
            }
          }
        ]
      });
      confirm.present();
    });
  }

  showProfile(userId: string) {
    let shoutModal = this.modalCtrl.create(ShoutDetailsPage, { userId: userId });
    shoutModal.present();
  }
}


