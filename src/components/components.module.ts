import { NgModule } from '@angular/core';
import {IonicModule} from 'ionic-angular'

import { CommonModule } from '@angular/common';
import { MomentModule } from 'angular2-moment';

import { ChatMessageComponent } from './chatmessage/chatmessage';
import { SvgIconComponent } from './svgicon/svgicon';

import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    ChatMessageComponent,
    SvgIconComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    PipesModule,
    MomentModule 
  ],
  exports: [
    ChatMessageComponent,
    SvgIconComponent
  ]
})
export class ComponentsModule { } 
