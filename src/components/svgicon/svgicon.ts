import { Component, Input } from '@angular/core';

@Component({
  selector: 'svgicon',
  templateUrl: 'svgicon.html'
})
export class SvgIconComponent {

  @Input() icon: string;

  constructor() {
  }

  get svgIcon() {
    return `assets/img/icons.svg#icon-${this.icon}`;
  }

}
