import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

// import { ChatService } from '../../providers/providers';
import { Chat, ChatMessage, User } from '../../models/models';
import { CHAT_MESSAGE_TEXT, CHAT_MESSAGE_SUBJECT, CHAT_MESSAGE_USER_LEAVED, CHAT_MESSAGE_USER_DELETED } from '../../models/models';


@Component({
  selector: 'chatmessage',
  templateUrl: 'chatmessage.html'
})
export class ChatMessageComponent implements OnInit {

  @Input() chat: Chat;
  @Input() message: ChatMessage;
  @Input() userId: string;

  @Output() clickAvatar = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    // consturuct message from chat !
    if (this.chat) {
      let user: User = { name: this.chat.name, image: this.chat.image }
      this.message = { type: CHAT_MESSAGE_SUBJECT, userId: this.chat.ownerId, createDate: this.chat.createDate, message: this.chat.subject, user: user };
    }
  }

  get isOwner(): boolean {
    return this.message.userId === this.userId;
  }

  public isUserLeaved(msg: ChatMessage): boolean {
    return msg.type == CHAT_MESSAGE_USER_LEAVED;
  }

  public isUserDeleted(msg: ChatMessage): boolean {
    return msg.type == CHAT_MESSAGE_USER_DELETED;
  }

  public isMessageText(msg: ChatMessage): boolean {
    return msg.type == CHAT_MESSAGE_TEXT;
  }

  /*
    public isShoutMessage(msg: ChatMessage): boolean {
      return msg.type == CHAT_MESSAGE_SHOUT;
    }
  */

  public getMessage(msg: ChatMessage): string {
    let value;
    switch (msg.type) {
      case CHAT_MESSAGE_USER_LEAVED:
        value = `${msg.user ? msg.user.name : ''} hat den Chat verlassen`;
        break;
      case CHAT_MESSAGE_USER_DELETED:
        value = `${msg.user ? msg.user.name : ''} wurde aus dem Chat gelöscht`;
        break;
      default:
        value = msg.message;
        break;
    }
    return value;
  }

  public getMessageClass(msg: ChatMessage): string {
    let clazz = "";
    switch (msg.type) {
      case CHAT_MESSAGE_SUBJECT:
        clazz = this.isOwner ? 'shout-mine' : 'shout-other';
        break;
      case CHAT_MESSAGE_TEXT:
        clazz = this.isOwner ? 'message-mine' : 'message-other';
        break;
      case CHAT_MESSAGE_USER_LEAVED:
        clazz = 'message-leaved';
        break;
      case CHAT_MESSAGE_USER_DELETED:
        clazz = 'message-deleted';
        break;
      default:
        clazz = this.isOwner ? 'message-mine' : 'message-other';
        break;
    }


    return clazz;
  }

  public clickProfile(userId: string) {
    this.clickAvatar.next(userId);
  }

}


