import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'age',
})
export class AgePipe implements PipeTransform {
  transform(value: string, ...args) {
    let result = "";
    console.log("In Age pipe = ", value)
    if (value != null) {
      result = moment().diff(moment(value, '"MM/DD/YYYY"'), 'years').toString();
    }
    return result;
  }
}
