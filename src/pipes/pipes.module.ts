import { NgModule } from '@angular/core';

import {CloudinaryImgPipe} from './cl-img';
import {PrettyPipe} from './pretty';
import {AgePipe} from './age';

@NgModule({
  declarations: [
    CloudinaryImgPipe,
    PrettyPipe,
    AgePipe
  ],
  imports: [
  ],
  exports: [
    CloudinaryImgPipe,
    PrettyPipe,
    AgePipe
  ]
})
export class PipesModule {}
