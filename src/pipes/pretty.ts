import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pretty',
})
export class PrettyPipe implements PipeTransform {
  transform(value: string, ...args) {
    let result = "";
    if (value != null) {
      result = value.replace(/(?:\r\n|\r|\n)/g, '<br />');
    }
    return result;
  }
}
