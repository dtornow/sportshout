import { Injectable, Pipe } from '@angular/core';

import { ImageService } from '../providers/image-service';

@Pipe({
  name: 'climg'
})
@Injectable()
export class CloudinaryImgPipe {
  /*
    Takes a value and makes it lowercase.
   */
  transform(value: string, isThumb:boolean=false) {
    let img;
    value = value || "m";
    switch(value) {
      case 'm':
        img =`assets/img/boy${isThumb?'_t':''}.png`;
        break;
      case 'f':
        img = `assets/img/girl${isThumb?'_t':''}.png`
        break;
      default: 
        img = `${ImageService.CLAUDINARY_RES_URL}/image/upload/${isThumb?'t_thumb/':''}${value}`;
    }
  
    return img;
  }
}
